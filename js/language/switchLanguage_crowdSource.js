function setLanguage(selectBoxId, textBoxId) {
  console.log("setLanguage called");
    pramukhIME.addLanguage(PramukhIndic);
    pramukhIME.enable();
    pramukhIME.onLanguageChange(scriptChangeCallback);
    var lang = (getCookie('pramukhime_language', ':english')).split(':');
    pramukhIME.setLanguage(lang[1], lang[0]);

    $("#" + selectBoxId).change(function () {
        $("#" + textBoxId).val("");
    }).change();

    $("#" + textBoxId).click(function () {
        var v = $("#" + selectBoxId).val();
        changeLanguage(get_related_lang_val(v));
    });
}

function setLanguageAdvancedSearch(selectBoxId){
  pramukhIME.addLanguage(PramukhIndic);
  pramukhIME.enable();
  pramukhIME.onLanguageChange(scriptChangeCallback);
  var lang = (getCookie('pramukhime_language', ':english')).split(':');
  pramukhIME.setLanguage(lang[1], lang[0]);

  $("#" + selectBoxId).change(function () {
      var v = $("#" + selectBoxId).val();
      changeLanguage(get_related_lang_val(v));
  }).change();
}

function get_related_lang_val(selectedLangString) {
    var val;
    var selectedLang = parseInt(selectedLangString);
    if (selectedLang === 10) { //assamese
        val = "pramukhindic:assamese";
    } else if (selectedLang === 19) {//bengali
        val = "pramukhindic:bengali";
    } else if (selectedLang === 40) { //english
        val = "pramukhindic:english";
    } else if (selectedLang === 58) { //gujarati
        val = "pramukhindic:gujarati";
    } else if (selectedLang === 63) {//hindi
        val = "pramukhindic:hindi";
    } else if (selectedLang === 80) {//kannada
        val = "pramukhindic:kannada";
    } else if (selectedLang === 104) {//malayalam
        val = "pramukhindic:malayalam";
    } else if (selectedLang === 106) {//marathi
        val = "pramukhindic:marathi";
    } else if (selectedLang === 123) {//oriya
        val = "pramukhindic:oriya";
    } else if (selectedLang === 126) {//punjabi
        val = "pramukhindic:punjabi";
    } else if (selectedLang === 138) {//sanskrit
        val = "pramukhindic:sanskrit";
    } else if (selectedLang === 145) {//sindhi
        val = "pramukhindic:sindhi";
    } else if (selectedLang === 156) {//tamil
        val = "pramukhindic:tamil";
    } else if (selectedLang === 158) {//telugu
        val = "pramukhindic:telugu";
    } else if (selectedLang === 185) {//konkani
        val = "pramukhindic:konkani";
    } else if (selectedLang === 186) {//manipuri
        val = "pramukhindic:manipuri";
    }
    return val;
}
