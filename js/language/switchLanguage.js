$(document).ready(function() {

    pramukhIME.addLanguage(PramukhIndic);

    pramukhIME.enable();
    pramukhIME.onLanguageChange(scriptChangeCallback);
    var lang = (getCookie('pramukhime_language', ':english')).split(':');
    pramukhIME.setLanguage(lang[1], lang[0]);

    var prevLang = "English";
    var clicked_lang = "English";
    //var searchElement;
    try {
        $('body').on('click', 'a.link1', function() {
            var all_lang_arr = ["English", "हिंदी", "অসমীয়া", "বাংলা", "ગુજરાતી", "ಕನ್ನಡ", "कोंकणी", "മലയാളം", "मणिपुरी", "मराठी", "ଓଡ଼ିଆ", "ਪੰਜਾਬੀ", "संस्कृत", "सिन्धी", "தமிழ்", "తెలుగు"];

            Array.prototype.remove = function(value) {
                var idx = this.indexOf(value);
                if (idx != -1) {
                    return this.splice(idx, 1); // The second parameter is the number of elements to remove.
                }
                return false;
            }
            $("a.link1.active").removeClass("active");
            $(this).addClass("active");
            prevLang = localStorage.getItem("clicked_prev_lang");
            clicked_lang = get_related_lang_val_reverse($(this).attr("data-lang"));
            localStorage.setItem("clicked_prev_lang", clicked_lang);

            $('#searchBox').focus();
            $('#searchBox').val("");
            console.log("Lang", $(this).attr("data-lang"));
            var v = $(this).attr("data-lang");
            //console.log("changeLanguage=",v);
            changeLanguage(v);
        });
    } catch (err) {
        console.log("ERr", err);
    }

    function get_related_lang_val(selectedLang) {
        var val;
        if (selectedLang == "English") {
            val = ":english";
        } else if (selectedLang == "हिंदी") {
            val = "pramukhindic:hindi";
        } else if (selectedLang == "অসমীয়া") {
            val = "pramukhindic:assamese";
        } else if (selectedLang == "বাংলা") {
            val = "pramukhindic:bengali";
        } else if (selectedLang == "ગુજરાતી") {
            val = "pramukhindic:gujarati";
        } else if (selectedLang == "ಕನ್ನಡ") {
            val = "pramukhindic:kannada";
        } else if (selectedLang == "कोंकणी") {
            val = "pramukhindic:konkani";
        } else if (selectedLang == "മലയാളം") {
            val = "pramukhindic:malayalam";
        } else if (selectedLang == "मणिपुरी") {
            val = "pramukhindic:manipuri";
        } else if (selectedLang == "मराठी") {
            val = "pramukhindic:marathi";
        } else if (selectedLang == "ଓଡ଼ିଆ") {
            val = "pramukhindic:oriya";
        } else if (selectedLang == "ਪੰਜਾਬੀ") {
            val = "pramukhindic:punjabi";
        } else if (selectedLang == "संस्कृत") {
            val = "pramukhindic:sanskrit";
        } else if (selectedLang == "सिन्धी") {
            val = "pramukhindic:sindhi";
        } else if (selectedLang == "தமிழ்") {
            val = "pramukhindic:tamil";
        } else if (selectedLang == "తెలుగు") {
            val = "pramukhindic:telugu";
        }

        return val;
    }

    function get_related_lang_val_reverse(selectedLang) {
        var val;
        if (selectedLang == ":english") {
            val = "English";
        } else if (selectedLang == "pramukhindic:hindi") {
            val = "हिंदी";
        } else if (selectedLang == "pramukhindic:assamese") {
            val = "অসমীয়া";
        } else if (selectedLang == "pramukhindic:bengali") {
            val = "বাংলা";
        } else if (selectedLang == "pramukhindic:gujarati") {
            val = "ગુજરાતી";
        } else if (selectedLang == "pramukhindic:kannada") {
            val = "ಕನ್ನಡ";
        } else if (selectedLang == "pramukhindic:konkani") {
            val = "कोंकणी";
        } else if (selectedLang == "pramukhindic:malayalam") {
            val = "മലയാളം";
        } else if (selectedLang == "pramukhindic:manipuri") {
            val = "मणिपुरी";
        } else if (selectedLang == "pramukhindic:marathi") {
            val = "मराठी";
        } else if (selectedLang == "pramukhindic:oriya") {
            val = "ଓଡ଼ିଆ";
        } else if (selectedLang == "pramukhindic:punjabi") {
            val = "ਪੰਜਾਬੀ";
        } else if (selectedLang == "pramukhindic:sanskrit") {
            val = "संस्कृत";
        } else if (selectedLang == "pramukhindic:sindhi") {
            val = "सिन्धी";
        } else if (selectedLang == "pramukhindic:tamil") {
            val = "தமிழ்";
        } else if (selectedLang == "pramukhindic:telugu") {
            val = "తెలుగు";
        }
        return val;
    }


});

function getLanguage(selectedLang) {
    var val;
    if (selectedLang == "English" || selectedLang == "en") {
        val = ":english";
    } else if (selectedLang == "हिंदी" || selectedLang == "hi") {
        val = "pramukhindic:hindi";
    } else if (selectedLang == "অসমীয়া" || selectedLang == "as") {
        val = "pramukhindic:assamese";
    } else if (selectedLang == "বাংলা" || selectedLang == "bn") {
        val = "pramukhindic:bengali";
    } else if (selectedLang == "ગુજરાતી" || selectedLang == "gu") {
        val = "pramukhindic:gujarati";
    } else if (selectedLang == "ಕನ್ನಡ" || selectedLang == "kn") {
        val = "pramukhindic:kannada";
    } else if (selectedLang == "कोंकणी" || selectedLang == "ni") {
        val = "pramukhindic:konkani";
    } else if (selectedLang == "മലയാളം" || selectedLang == "ml") {
        val = "pramukhindic:malayalam";
    } else if (selectedLang == "मणिपुरी" || selectedLang == "ma") {
        val = "pramukhindic:manipuri";
    } else if (selectedLang == "मराठी" || selectedLang == "mr") {
        val = "pramukhindic:marathi";
    } else if (selectedLang == "ଓଡ଼ିଆ" || selectedLang == "or") {
        val = "pramukhindic:oriya";
    } else if (selectedLang == "ਪੰਜਾਬੀ" || selectedLang == "pa") {
        val = "pramukhindic:punjabi";
    } else if (selectedLang == "संस्कृत" || selectedLang == "sa") {
        val = "pramukhindic:sanskrit";
    } else if (selectedLang == "सिन्धी" || selectedLang == "sd") {
        val = "pramukhindic:sindhi";
    } else if (selectedLang == "தமிழ்" || selectedLang == "ta") {
        val = "pramukhindic:tamil";
    } else if (selectedLang == "తెలుగు" || selectedLang == "tel") {
        val = "pramukhindic:telugu";
    }
    return val;
}
