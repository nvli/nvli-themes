// Carousel Auto-Cycle
//  $(document).ready(function() {
// // invoke the carousel
//$('#myCarousel').carousel({
//  interval: 3000
//});
//  });



$(document).ready(function () {

    var context1 = (typeof __NVLI !== 'undefined') ? __NVLI.context + "/themes/" : "";

    //min font size
    //min font size
    var min = 11;

    //max font size
    var max = 16;

    //grab the default font size
    var reset = $('body').css('fontSize');
    var reset1 = $('.fix_block1 > div > p').css('fontSize');

    //font resize these elements
    var elm = $('body');
    var elz = $('.fix_block1 > div > p');

    //set the default font size and remove px from the value
    var size = str_replace(reset, 'px', '');

    //Increase font size
    $('a.fontSizePlus').click(function () {

        //if the font size is lower or equal than the max value
        if (size <= max) {

            //increase the size
            size++;

            //set the font size
            elm.css({
                'fontSize': size
            });
            elz.css({
                'fontSize': size
            });
        }

        //cancel a click event
        return false;

    });

    $('a.fontSizeMinus').click(function () {
        //if the font size is greater or equal than min value
        if (size >= min) {
            //decrease the size
            size--;

            //set the font size
            elm.css({
                'fontSize': size
            });
            elz.css({
                'fontSize': size
            });
        }

        //cancel a click event
        return false;

    });

    //Reset the font size
    $('a.fontReset').click(function () {

        //set the default font size
        elm.css({
            'fontSize': reset
        });
        elz.css({
            'fontSize': reset1
        });
    });

    // //A string replace function
    function str_replace(haystack, needle, replacement) {
        var temp = haystack.split(needle);
        return temp.join(replacement);
    }

    $('#drop_down_menu a').click(function () {
        $('#selected').text($(this).text());
    });

    /*$("#default_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/default.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/default.css');
     localStorage.setItem("selectedTheme", "default_theme");
     });
     
     $("#black_theme").click(function () {
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/dark.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/dark.css');
     localStorage.setItem("selectedTheme", "black_theme");
     });
     
     $("#light_theme").click(function () {
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/light.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/light.css');
     localStorage.setItem("selectedTheme", "light_theme");
     });
     
     $("#lord_ganesha_theme").click(function () {
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/lord_ganesha.css');
     localStorage.setItem("selectedTheme", "lord_ganesha_theme");
     });
     
     $("#gandhi_jayanti_theme").click(function () {
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/mahatma_gandhi.css');
     localStorage.setItem("selectedTheme", "gandhi_jayanti_theme");
     });
     
     $("#olympic_theme").click(function () {
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/olympic.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/olympic.css');
     
     localStorage.setItem("selectedTheme", "olympic_theme");
     });
     
     $("#nag_panchmi_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/nag_panchmi.css');
     localStorage.setItem("selectedTheme", "nag_panchmi_theme");
     });
     
     $("#mahashivratri_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/mahashivratri.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/mahashivratri.css');
     localStorage.setItem("selectedTheme", "mahashivratri_theme");
     });
     
     $("#gudi_padwa_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/gudi_padwa.css');
     localStorage.setItem("selectedTheme", "gudi_padwa_theme");
     });
     
     $("#independence_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/independence_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/independence_day.css');
     localStorage.setItem("selectedTheme", "independence_day_theme");
     });
     
     $("#mahavir_jayanti_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/mahavir_jayanti.css');
     localStorage.setItem("selectedTheme", "mahavir_jayanti_theme");
     });
     
     $("#muharram_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/muharram.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/muharram.css');
     localStorage.setItem("selectedTheme", "muharram_theme");
     });
     
     $("#yogaday_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/yoga_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/yoga_day.css');
     localStorage.setItem("selectedTheme", "yogaday_theme");
     });
     
     $("#navratri_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/navratri.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/navratri.css');
     localStorage.setItem("selectedTheme", "navratri_theme");
     });
     
     $("#basant_panchami_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/basant_panchami.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/basant_panchami.css');
     localStorage.setItem("selectedTheme", "basant_panchami_theme");
     });
     
     $("#republic_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/republic_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/republic_day.css');
     localStorage.setItem("selectedTheme", "republic_day_theme");
     });
     
     $("#diwali_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/diwali.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/diwali.css');
     localStorage.setItem("selectedTheme", "diwali_theme");
     });
     
     $("#buddha_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/buddha_jayanti.css');
     localStorage.setItem("selectedTheme", "buddha_theme");
     });
     
     $("#mother_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/diwali.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/mother_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/mother_day.css');
     localStorage.setItem("selectedTheme", "mother_day_theme");
     });
     
     $("#earth_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/earth_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/earth_day.css');
     localStorage.setItem("selectedTheme", "earth_day_theme");
     });
     
     $("#ambedkar_jayanti_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/ambedkar_jayanti.css');
     localStorage.setItem("selectedTheme", "ambedkar_jayanti_theme");
     });
     
     $("#netaji_jayanti_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/netaji_jayanti.css');
     localStorage.setItem("selectedTheme", "netaji_jayanti_theme");
     });
     
     $("#rabindra_jayanti_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/rabindra_jayanti.css');
     localStorage.setItem("selectedTheme", "rabindra_jayanti_theme");
     });
     
     $("#vaccination_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/vaccination_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/vaccination_day.css');
     localStorage.setItem("selectedTheme", "vaccination_day_theme");
     });
     
     $("#voters_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/voters_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/voters_day.css');
     localStorage.setItem("selectedTheme", "voters_day_theme");
     });
     
     $("#fathers_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/fathers_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/fathers_day.css');
     localStorage.setItem("selectedTheme", "fathers_day_theme");
     });
     
     $("#workers_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/workers_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/workers_day.css');
     localStorage.setItem("selectedTheme", "workers_day_theme");
     });
     
     $("#teachers_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/teachers_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/teachers_day.css');
     localStorage.setItem("selectedTheme", "teachers_day_theme");
     });
     
     $("#sports_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/sports_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/sports_day.css');
     localStorage.setItem("selectedTheme", "sports_day_theme");
     });
     
     $("#holi_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/holi.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/holi.css');
     
     localStorage.setItem("selectedTheme", "holi_theme");
     });
     
     $("#post_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/post_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/post_day.css');
     
     localStorage.setItem("selectedTheme", "post_day_theme");
     });
     
     $("#air_force_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/air_force_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/air_force_day.css');
     
     localStorage.setItem("selectedTheme", "air_force_day_theme");
     });
     
     $("#kisan_divas_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/kisan_diwas.css');
     localStorage.setItem("selectedTheme", "kisan_divas_theme");
     });
     
     $("#navy_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/navy_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/navy_day.css');
     localStorage.setItem("selectedTheme", "navy_day_theme");
     });
     
     $("#childrens_day_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/childrens_day.css');
     $('link[href="' + context1 + 'css/rama_navami.css"]').attr('href', context1 + 'css/childrens_day.css');
     localStorage.setItem("selectedTheme", "childrens_day_theme");
     });
     
     $("#rama_navami_theme").click(function () {
     $('link[href="' + context1 + 'css/dark.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/light.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/mahatma_gandhi.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/lord_ganesha.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/olympic.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/default.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/nag_panchmi.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/mahashivratri.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/gudi_padwa.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/independence_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/mahavir_jayanti.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/muharram.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/yoga_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/navratri.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/basant_panchami.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/republic_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/buddha_jayanti.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/mother_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/earth_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/ambedkar_jayanti.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/netaji_jayanti.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/rabindra_jayanti.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/vaccination_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/voters_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/fathers_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/workers_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/teachers_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/sports_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/holi.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/post_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/air_force_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/kisan_diwas.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/navy_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     $('link[href="' + context1 + 'css/childrens_day.css"]').attr('href', context1 + 'css/rama_navami.css');
     localStorage.setItem("selectedTheme", "rama_navami_theme");
     });*/



    switch (localStorage.getItem("selectedTheme")) {
        case "default_theme":
            $("#default_theme").trigger("click");
            break;
        case "black_theme":
            $("#black_theme").trigger("click");
            break;
        case "light_theme":
            $("#light_theme").trigger("click");
            break;
        case "lord_ganesha_theme":
            $("#lord_ganesha_theme").trigger("click");
            break;
        case "gandhi_jayanti_theme":
            $("#gandhi_jayanti_theme").trigger("click");
            break;
        case "olympic_theme":
            $("#olympic_theme").trigger("click");
            break;
        case "independence_day_theme":
            $("#independence_day_theme").trigger("click");
            break;
        case "gudi_padwa_theme":
            $("#gudi_padwa_theme").trigger("click");
            break;
        case "mahashivratri_theme":
            $("#mahashivratri_theme").trigger("click");
            break;
        case "mahavir_jayanti_theme":
            $("#mahavir_jayanti_theme").trigger("click");
            break;
        case "nag_panchmi_theme":
            $("#nag_panchmi_theme").trigger("click");
            break;
        case "muharram_theme":
            $("#muharram_theme").trigger("click");
            break;
        case "yogaday_theme":
            $("#yogaday_theme").trigger("click");
            break;
        case "navratri_theme":
            $("#navratri_theme").trigger("click");
            break;
        case "basant_panchami_theme":
            $("#basant_panchami_theme").trigger("click");
            break;
        case "republic_day_theme":
            $("#republic_day_theme").trigger("click");
            break;
        case "diwali_theme":
            $("#diwali_theme").trigger("click");
            break;
        case "buddha_theme":
            $("#buddha_theme").trigger("click");
            break;
        case "mother_day_theme":
            $("#mother_day_theme").trigger("click");
            break;
        case "earth_day_theme":
            $("#earth_day_theme").trigger("click");
            break;
        case "ambedkar_jayanti_theme":
            $("#ambedkar_jayanti_theme").trigger("click");

            break;
        case "netaji_jayanti_theme":
            $("#netaji_jayanti_theme").trigger("click");
            break;
        case "rabindra_jayanti_theme":
            $("#rabindra_jayanti_theme").trigger("click");
            break;
        case "vaccination_day_theme":
            $("#vaccination_day_theme").trigger("click");
            break;
        case "voters_day_theme":
            $("#voters_day_theme").trigger("click");
            break;
        case "fathers_day_theme":
            $("#fathers_day_theme").trigger("click");
            break;
        case "workers_day_theme":
            $("#workers_day_theme").trigger("click");
            break;
        case "teachers_day_theme":
            $("#teachers_day_theme").trigger("click");
            break;
        case "sports_day_theme":
            $("#sports_day_theme").trigger("click");
            break;
        case "holi_theme":
            $("#holi_theme").trigger("click");
            break;
        case "post_day_theme":
            $("#post_day_theme").trigger("click");
            break;
        case "air_force_day_theme":
            $("#air_force_day_theme").trigger("click");
            break;
        case "kisan_divas_theme":
            $("#kisan_divas_theme").trigger("click");
            break;
        case "navy_day_theme":
            $("#navy_day_theme").trigger("click");
            break;
        case "childrens_day_theme":
            $("#childrens_day_theme").trigger("click");
            break;
        case "rama_navami_theme":
            $("#rama_navami_theme").trigger("click");
            break;
        case "default_new_theme":
            $("#default_new_theme").trigger("click");
            break;
        case "independence_day_new_theme":
            $("#independence_day_new_theme").trigger("click");
            break;
        case "raksha_bandhan_theme":
            $("#raksha_bandhan_theme").trigger("click");
            break;
        case "default_light_theme":
            $("#default_light_theme").trigger("click");
            break;
        case "army_day_theme":
            $("#army_day_theme").trigger("click");
            break;
        case "forest_day_theme":
            $("#forest_day_theme").trigger("click");
            break;
        case "science_day_theme":
            $("#science_day_theme").trigger("click");
            break;
        case "youth_day_theme":
            $("#youth_day_theme").trigger("click");
            break;

        default:
            $("#default_theme").trigger("click");
            break;
    }
});

$(document).ready(function () {
    $('#menu').click(function (event) {
        $('.m-t-3').css('position', 'relative');
        $('.m-t-3').css('z-index', '-1');
        $('#main').css('position', 'relative');
        $('#main').css('z-index', '-1');
        $('.arrow_header').slideToggle('slow');
        $('#gridbox').slideToggle('slow', function () {
            $('ul').animate({
                marginTop: '0'
            });
            $('#gridbox').animate({
                height: '520px'
            });
            $('#back_header').text('Less').animate({
                marginTop: '0'
            });
            $('#more').show();
            $('#more').text('More').animate({
                marginTop: '0'
            });
        });
        event.stopPropagation();
    });

    $('#more').click(function (event) {
        $('ul#icons').animate({
            marginTop: '-470px'
        }, 200);
        $('#gridbox').animate({
            height: '520px'
        }, 200);
        $('#more').hide();
        $('#back_header').show();
        $('#back_header').text('Less').animate({
            marginTop: '0'
        });
        event.stopPropagation();
    });

    $('#back_header').click(function (event) {
        $('ul#icons').animate({
            marginTop: 0
        }, 200);
        $('#more').show();
        $('#gridbox').animate({
            height: '520px'
        }, 200);
        $('#more').text('More').animate({
            marginTop: '0'
        });
        $('#back_header').hide();
        event.stopPropagation();
    });

    $('body').click(function () {
        $('.m-t-3').css('z-index', '1');
        $('#main').css('z-index', '1');
        $('.arrow_header').hide();
        $('#gridbox').hide();
        $('ul').animate({
            marginTop: '0'
        });
        $('#gridbox').animate({
            height: '520px'
        });
        $('#more').text('More').animate({
            marginTop: '0'
        });
        $('#back_header').hide();
    });
});


jQuery(document).ready(function (  ) {
    // hold onto the drop down menu
    var dropdownMenu;

    // and when you show it, move it to the body
    jQuery(window).on('show.bs.dropdown', function (e) {
        // grab the menu
        dropdownMenu = jQuery(e.target).find('.dropdown-menu');
        // detach it and append it to the body
        jQuery('body').append(dropdownMenu.detach());

        // grab the new offset position
        var eOffset = jQuery(e.target).offset();

        // make sure to place it where it would normally go (this could be improved)
        dropdownMenu.css({
            'display': 'inline-block',
            'top': eOffset.top + 30,
            'left': eOffset.left
        });
    });

    // and when you hide it, reattach the drop down, and hide it normally
    jQuery(window).on('hide.bs.dropdown', function (e) {
        jQuery(e.target).append(dropdownMenu.detach());
        dropdownMenu.hide();
    });
});

/* $('#scrollbox4').enscroll({
 showOnHover: true,
 verticalTrackClass: 'track3',
 verticalHandleClass: 'handle3'
 });*/