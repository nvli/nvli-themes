


$(document).ready(function(){
	$(window).resize(function(){
		//alert("resized");
		console.log("resize");
		location.reload();
	});


	sly_horizontal();

	var titleText=$("#menuInfo h2").text();
	//alert(titleText);

	$(".frame ul li img").click(function(){
		//alert("click");
		/*console.log("clicked menu");
		console.log($(this).attr("src"));

		var tooltipTxt=$(this).parent().attr("title");
		$("#menuInfo h2").text(tooltipTxt);
		console.log(tooltipTxt);
		var animationName="animated fadeInUp";
		var animationend="webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationEnd oanimationEnd";
		$("#menuInfo").addClass(animationName).one(animationend,function(){
			$(this).removeClass(animationName);
		});*/

		// var animationName_click="animated pulse";
		// var animationend="webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationEnd oanimationEnd";
		// $(this).addClass(animationName_click).one(animationend,function(){
		// 	$(this).removeClass(animationName_click);
		// });
	});

/*$(".frame ul li img :hover").click(function(){
	getLabel(0);
});
	function getLabel(li_num){
		var li_num=li_num+1;
		var active_li_is=$(".frame ul li:nth-child("+li_num+")");
		var title_is=active_li_is.attr("title");
		//console.log("Title",title_is);

		$("#menuInfo h2").text(title_is);
		var animationName="animated fadeInUp";
		var animationend="webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationEnd oanimationEnd";
		$("#menuInfo").addClass(animationName).one(animationend,function(){
			$(this).removeClass(animationName);
		});

	}*/

	// $(".frame ul li").each(function(){
	// 	console.log($(this).hasClass('active'));
	// 	if($(this).hasClass('active')){
	// 		var animationName_click="animated pulse";
	// 		var animationend="webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationEnd oanimationEnd";
	// 		$(this).addClass(animationName_click).one(animationend,function(){
	// 			$(this).removeClass(animationName_click);
	// 		});
	// 	}
	// });



	function sly_horizontal(){
		var $frame = $('#cycleitems');
		var $wrap  = $frame.parent();

		// Call Sly on frame
		$frame.sly({
			horizontal: 1,
			itemNav: 'centered',//changed from basic
			smart: 1,
			activateOn: 'click',
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 0,
			scrollBar: $wrap.find('.scrollbar'),
			scrollBy: 1,
			speed: 300,
			elasticBounds: 1,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,

			// Cycling
			cycleBy: 'items',
			cycleInterval: 2000,
			pauseOnHover: 1,

			// Buttons
			//prev: $wrap.find('.prev'),
			//next: $wrap.find('.next')
		});

		// Pause button
		$wrap.find('.pause').on('click', function () {
			$frame.sly('pause');
		});

		// Resume button
		$wrap.find('.resume').on('click', function () {
			$frame.sly('resume');
		});

		// Toggle button
		$wrap.find('.toggle').on('click', function () {
			$frame.sly('toggle');
		});

		$("#prevBtn").on('click', function () {
			$frame.sly('prev');
		});

		$("#nextBtn").on('click', function () {
			$frame.sly('next');
		});
	}

	// -------------------------------------------------------------
	//   One Item Per Frame
	// -------------------------------------------------------------
	(function () {
		var $frame = $('#oneperframe');
		var $wrap  = $frame.parent();

		$("#prev_button").on('click', function () {
			$frame.sly('prev');
		});

		$("#next_button").on('click', function () {
			$frame.sly('next');
		});
		// Call Sly on frame
		$frame.sly({
			horizontal: 1,
			itemNav: 'forceCentered',
			smart: 1,
			activateMiddle: 1,
			mouseDragging: 1,
			touchDragging: 1,
			releaseSwing: 1,
			startAt: 0,
			scrollBar: $wrap.find('.scrollbar'),
			scrollBy: 1,
			speed: 300,
			elasticBounds: 1,
			easing: 'easeOutExpo',
			dragHandle: 1,
			dynamicHandle: 1,
			clickBar: 1,
            
            // Cycling
			cycleBy: 'items',
			cycleInterval: 1500,
			pauseOnHover: 1
		});
	}());
});
