CKEDITOR.plugins.add('akinzacropify', {
    requires: 'dialog,image',
    icons: 'crop_add',
//    lang: 'en',
    init: function (editor) {
        var pluginDirectory = this.path, style = document.createElement('link'),
            script = document.createElement('script');
        
        // Attach Scripts and CSS to DOM
        style.type = 'text/css';
        style.rel = 'stylesheet';
        style.href = editor.config.cropperCSS;
        document.head.appendChild(style);
        
        script.type = "text/javascript";
        script.src = editor.config.cropperJS;
        document.head.appendChild(script);
        //Plugin logic goes here.
        editor.addCommand('imgUploader', {
            exec: function (editor) {
                console.log(editor);
                imageBrowser();
            }
        });
        editor.ui.addButton('UploadImage', {
            label: 'Upload and Insert Image',
            command: 'imgUploader',
            toolbar: 'insert,100',
            icon: this.path + 'icons/crop_add.png'
        });
    }
});
