CKEDITOR.plugins.setLang('akinzaimageuploader', 'en', {
    cropTab: 'Crop Image',
    title: 'Upload Browse & Crop Image',
    btnUpload: 'Upload Image',
    wrongImageType: 'Please choose an image file.'
});
