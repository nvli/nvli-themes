CKEDITOR.plugins.add('timestamp', {
    icons: 'clock_red',
    init: function (editor) {
        //Plugin logic goes here.
        editor.addCommand('insertTimestamp', {
            exec: function (editor) {
                var now = new Date();
                editor.insertHtml('Date & Time : <strong> <em>' + now.toLocaleString() + '</em></strong>');
            }
        });

        editor.ui.addButton('Timestamp', {
            label: 'Insert Timestamp',
            command: 'insertTimestamp',
            toolbar: 'insert,100',
            icon: this.path + 'icons/clock_red.png'
            
        });
    }
});
