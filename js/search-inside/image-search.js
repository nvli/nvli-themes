$(document).ready(function(){

  var canvas = new fabric.Canvas('c');
  var bboxes = [];
  var paragraphs = null;
  var index = lunr(function () {
    this.field('text');
    this.ref('id');
  });

	var base_image = new Image();
	base_image.src = imagePath;
	
	
  base_image.onload = function(){
    canvas.setDimensions({width: this.width/3, height: this.height/3});
    var img = new fabric.Image(base_image, {
      left: 0,
      top: 0,
      width: this.width/3,
      height: this.height/3
    });
    canvas.setBackgroundImage(img, canvas.renderAll.bind(canvas));
  }

  $('input#image-search').on('keyup', function () {
    bboxes.forEach(function(rect){
      canvas.remove(rect);
    });
    bboxes = [];
    // Get query
    var queries = $(this).val().split(" ");
    queries.forEach(function(query){
        // Search for it
        var results = index.search(query);
        results.forEach(function(result){

          var paragraph = paragraphs[result.ref];
          var rect = new fabric.Rect({
            left: paragraph.x/3,
            top: paragraph.y/3,
            fill: "rgba(255, 66, 66, 0.5)",
            width: paragraph.w/3,
            height: paragraph.h/3,
            lockMovementX: true,
            lockMovementY: true,
            hasControls: false
          });
          canvas.add(rect);  
          bboxes.push(rect);
        });
    });
  });

  $.ajax({
    url: jsonPath,
    cache: true,
    method: 'GET',
    success: function(data) {
      data = data;   
      paragraphs = data;
      var keys = Object.keys(data);
      keys.forEach(function(key){
        index.add({
          id: key,
          text: data[key].text
        });
      });
    },error: function(dada) {
    }
  });

});
