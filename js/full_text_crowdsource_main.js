'use strict';

Util.onReady(function () {
    var hocrProofreader = new HocrProofreader({
        layoutContainer: 'layout-container',
        editorContainer: 'editor-container',
		image:ImgName
    });

    

    document.getElementById('zoom-page-full').addEventListener('click', function () {
        hocrProofreader.setZoom('page-full');
    });

    document.getElementById('zoom-page-width').addEventListener('click', function () {
        hocrProofreader.setZoom('page-width');
    });

  document.getElementById('zoom-original').addEventListener('click', function () {
        hocrProofreader.setZoom('original');
    });

document.getElementById('button-save').addEventListener('click', function () {
        if (confirm("Do you really want to save this content?")) {
            var hocr = hocrProofreader.getHocr();
            var request = new XMLHttpRequest();
            request.open('POST', con + '/cs/full-text-save/' + recordName + '/' + htmlName);
            request.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    if(this.responseText == "OK"){
                        alert("Data saved successfully..")
                    }else{
                        alert("Error in saving page...")
                    }
                }
            };
            request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');
            request.send('hocr=' + encodeURIComponent(hocr));
        }
    });

    var hocrBaseUrl = hocrBase;
    var hocrUrl = hocrBaseUrl + hOCRName;

    Util.get(hocrUrl, function (err, hocr) {
        if (err) return Util.handleError(err);

        hocrProofreader.setHocr(hocr, hocrBaseUrl);
    });
});




