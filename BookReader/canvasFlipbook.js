// JavaScript Document
var imgPathArr = [];
var imgCounter = 0;
var playing_flag = false;
var nxtCount = 1,
        prev_selectBoxVal = 1;
var zoomBtnClicked = false;
var browser_is_firefox = false,
        go_clicked_flag = false;
var imagePathJsonObject = {};
browser_is_firefox = detectBrowser_is_firefox();

var pageWidthMy, pageHeightMy;
var tempCanvas = null,
        canvas = null;
var tempContext = null,
        context = null;
var holder = null;
var play_pause_btn_clicked = false;

/*next and previous zoom start*/
var cur_img_index = 0;
/*next and previous zoom end*/

function scaledSize_small_canvas_New(imgWidth, imgHeight, pageWidth, pageHeight) {
    var canvas_Small_width = pageWidth;
    var canvas_Small_height = pageHeight;
    var widthScale = canvas_Small_width / imgWidth;
    var heightScale = canvas_Small_height / imgHeight;

    var scaleFactor;
    //The smaller scale factor will scale more (0 < scaleFactor < 1) leaving the other dimension inside the newSize rect
    (widthScale < heightScale) ? (scaleFactor = widthScale) : (scaleFactor = heightScale);

    return scaleFactor;
}

function detectBrowser_is_firefox() {
    var isFirefox = typeof InstallTrigger !== 'undefined'; // Firefox 1.0+
    return isFirefox;
}

$(window).resize(function () {
    try {
        load_Initial_Pages();
        //window.location = window.location.pathname;
    } catch (err) {
        console.log("resize", err);
    }
});

function getWindowWidth() {
    var myWidth = 0;
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myWidth = window.innerWidth;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myWidth = document.documentElement.clientWidth;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myWidth = document.body.clientWidth;
    }
    return myWidth;
}


function getWindowHeight() {
    var myHeight = 0;
    if (typeof (window.innerWidth) == 'number') {
        //Non-IE
        myHeight = window.innerHeight;
    } else if (document.documentElement && (document.documentElement.clientWidth || document.documentElement.clientHeight)) {
        //IE 6+ in 'standards compliant mode'
        myHeight = document.documentElement.clientHeight;
    } else if (document.body && (document.body.clientWidth || document.body.clientHeight)) {
        //IE 4 compatible
        myHeight = document.body.clientHeight;
    }
    return myHeight;
}


function load_Initial_Pages() {
    try {
        imgCounter = imgPathArr.length;
        //console.log("imges" + imgCounter);

        //var headerHt =$("div.full-width:first").height();
        //var headerHt=$("div.container_shadow:first").height()+$("div.comtainer-fluid menu_bg").height();
        var btnDivHeight = document.getElementById('fb-btn_div').clientHeight + 25;

        var footerHt = $("div#footer-wrapper").height();

        var borderMargin = 4;

        var u_div_wd = $("#bookDiv").width();
        var u_div_ht = $("#bookDiv").height() - (btnDivHeight) - borderMargin;


        $(canvasHolder).css("width", u_div_wd);
        $(canvasHolder).css("height", u_div_ht);

        //var margin_top = headerHt + btnDivHeight;
        $(canvasHolder).css("position", "absolute");
        //$(canvasHolder).css("top", margin_top);
        $(canvasHolder).css("top", "0px");
        //$(canvasHolder).css("top", "20px");
        //code to position canvasHolder div end


        $("#fb-btn_div").css("position", "absolute");
        var btn_div_top = $("#bookDiv").height() - 70;
        $("#fb-btn_div").css("top", btn_div_top + "px");
        var btn_div_left = (u_div_wd - $("#fb-btn_div").width()) / 2;
        $("#fb-btn_div").css("left", btn_div_left + "px");

        //code to create tempCanvas start
        var holder = $(canvasHolder);
        var holder1 = $(canvasHolder1);
        var imgCommonWidth, imgCommonHeight;
        //take image size of first image
        var img1 = new Image();
        img1.onload = function () {

            imgCommonWidth = this.width;
            imgCommonHeight = this.height;
            //console.log("Wid 1st image " + imgCommonWidth + " " + "Hei 1st image " + imgCommonHeight);
            var u_div_wd_minus_border = u_div_wd - (borderMargin * 2);
            var u_div_ht_minus_border = u_div_ht - (borderMargin * 2);

            var sf = scaledSize_small_canvas_New(imgCommonWidth, imgCommonHeight, (u_div_wd_minus_border / 2), u_div_ht_minus_border);
            //alert("A="+(u_div_wd_minus_border / 2)+"B="+u_div_ht_minus_border);
            var canvasWidth = (imgCommonWidth * sf);
            var canvasHeight = (imgCommonHeight * sf);

            //alert("W="+canvasWidth+"h="+canvasHeight);
            //console.log("canvasWidth"+canvasWidth+"canvasHeight"+canvasHeight);
            $(canvasHolder1).css("width", canvasWidth * 2);
            $(canvasHolder1).css("height", canvasHeight);
            //$(canvasHolder1).css('border', '4px solid black');
            var left_val = (u_div_wd - (canvasWidth * 2)) / 2;
            //var top_val = (u_div_ht - canvasHeight) / 2;
            //var margin_top = headerHt + btnDivHeight;
            $(canvasHolder1).css("position", "absolute");
            $(canvasHolder1).css("top", "0px");
            $(canvasHolder1).css("left", left_val);


            if ($("#mainCanvas").length == 0) {
                canvas = $("<canvas id='mainCanvas'></canvas>");
                canvas
                        .css({
                            position: 'absolute',
                            top: 0,
                            left: 0
                                    /*,
                                     transform:'rotate(90deg)'*/
                        })
                        .attr('width', (canvasWidth * 2))
                        .attr('height', canvasHeight)
                        .appendTo(holder1);
            }

            //alert("canvasWidth=="+canvasWidth+"==canvasHeight"+canvasHeight);



            if ($("#tempCanvas").length == 0) {
                tempCanvas = $("<canvas id='tempCanvas'></canvas>");
                tempCanvas
                        .css({
                            position: 'absolute',
                            top: 0,
                            left: 0
                                    /*,
                                     transform:'rotate(90deg)'*/
                        })
                        .attr('width', (canvasWidth * 2))
                        .attr('height', canvasHeight)
                        .appendTo(holder1);
            }

            context = canvas.get(0).getContext('2d');
            tempContext = tempCanvas.get(0).getContext('2d');
            //code to create tempCanvas end
            var canvas1 = document.getElementById("tempCanvas");
            var context1 = canvas1.getContext('2d');
            pageWidthMy = (canvas1.width / 2);
            pageHeightMy = canvas1.height;
            //console.log("page wd"+pageWidthMy+"page ht"+pageHeightMy);



            if ($("#liId" + 1).length == 0) {
                $('<li/>', {
                    'id': 'liId' + (1)
                }).prepend("<img src=" + imgPathArr[0] + ">").appendTo('#lstImages');
            }

            var isIPad = navigator.userAgent.indexOf('iPad') >= 0;
            $('#lstImages').pageFlipper({
                fps: isIPad ? 10 : 20,
                easing: isIPad ? 0.3 : 0.2,
                //backgroundColor: '#fdf9e7'
                backgroundColor: '#ffffff'
                        //backgroundColor: 'transparent'(0)                    //backgroundColor: 'inherit'
            });



        };
        img1.src = imgPathArr[0];



    } catch (err) {
        console.log("load_Initial_Pages", err);
    }
}


$.fn.pageFlipper = function (userOptions) {

    findAllPageLi(); //added 30-07-2015

    //accordianExpand(true); //added 31-07-2015 //call position changed 5-08-2015 //commented 6-05-2015

    var defaults = {
        className: 'canvasHolder',
        pageWidth: pageWidthMy,
        pageHeight: pageHeightMy,
        easing: 0.5,
        fps: 20,
        defaultPageColor: 'white',
        backgroundColor: 'white',
        cornerSide: 50
    };
    //pgwidth_value = pageWidthMy;
    var options = $.extend(defaults, userOptions);
    options.updateDelay = 1000 / options.fps;
    options.spreadWidth = options.pageWidth * 2;
    options.pageDiagonal = Math.sqrt(options.pageWidth * options.pageWidth + options.pageHeight * options.pageHeight);
    options.pageClipWidth = options.pageWidth;
    options.pageClipHeight = options.pageWidth * 10;
    options.pageHeightOffset = options.pageDiagonal - options.pageHeight;

    var mousePosition = null;
    var followerPosition = null;

    var pageAngle = 0;

    var spineTopX = options.pageWidth;
    var spineTopY = 0;
    var spineBottomTop = options.pageHeight;

    var bisectorTangetY = options.pageHeight;

    var leftIsActive;

    //my variables
    var cur_selectBoxVal = null;
    var canvas_Small_width = options.pageWidth,
            canvas_Small_height = options.pageHeight;

    function createHandles() {
        try {
            mousePosition = {
                top: options.pageHeight,
                left: options.pageWidth * 2
            };
            followerPosition = {
                top: options.pageHeight,
                left: options.pageWidth * 2
            };
            //leftIsActive = false;
            leftIsActive = true;//CHANGED 9-03-2016
        } catch (err) {
            console.log("createHandles", err);
        }
    }

    function createCanvas(element) {
        try {
            holder = $(canvasHolder);
        } catch (err) {
            console.log("createCanvas err" + err);
        }
    }

    //commented on 21-08-2015
    function getRealPosition(element, position) {
        element = $(element);
        if (position == null) {
            position = element.position();
        }

        return {
            top: position.top + element.height() / 2 - options.pageHeightOffset,
            left: position.left + element.width() / 2
        };
    }

    var dragging = false;
    var flipInProgress = false;
    var outerClick = false; // for fixing issue where clicking outside the page still triggers action //ADD 9-06-2015
    function activateMouseHandle() {
        $(tempCanvas)
                .bind('click', afterCanvasClick);
        /*.bind('mousemove', onCanvasHovering)
         .bind('mouseout', onCanvasHoverOut)
         .bind('mousedown', onCanvasActionStart)
         .bind('touchstart', onCanvasActionStart)*/
        ;

        /*$(document)
         .bind('mousemove', function(event) {
         //console.log("in canvas mousemove");
         nextMouseUpIsClick = false;
         if (!flipInProgress) {
         if (dragging) {
         if (!browser_is_firefox) {
         mousePosition = {
         left: (event.pageX - $(event.target).parent().offset().left) - $(holder).position().left,
         top: (event.pageY - $(event.target).parent().offset().top) - options.pageHeightOffset + 70
         };
         } else {
         mousePosition = {
         left: (event.pageX - $(event.target).parent().offset().left) - $(holder).position().left,
         top: (event.pageY - $(event.target).parent().offset().top) + 70
         };
         }
         }
         }
         //console.log("in canvas mousemove","left",mousePosition.left,"top",mousePosition.top);
         //when mouse is hovered on tempCanvas then make outerClick var false
         if (event.target.id == "tempCanvas") {
         outerClick = false;
         }
         })



         .bind('touchmove', function(event) {
         nextMouseUpIsClick = false;
         if (!flipInProgress) {
         if (dragging && event.originalEvent != null && event.originalEvent.touches.length == 1) {
         //event.preventDefault();
         var touch = event.originalEvent.touches[0];
         mousePosition = {
         left: touch.screenX - $(holder).position().left,
         top: touch.screenY - options.pageHeightOffset + 70
         };
         }
         }
         })
         .bind('mouseup', onCanvasActionStop)
         .bind('touchend', onCanvasActionStop);*/

        //move to next image on next button click
        $('#fb-nxt')
                .bind('click', function (event) {
                    pageTurnedflag = true;

                    zoomBtnClicked = false;
                    go_clicked_flag = false;
                    accordianClicked = false;

                    $("#canvasHolder1").css("cursor", "default");
                    stopAnimation();

                    var liId_index1 = nxtCount + 1;
                    var liId_index2 = nxtCount + 2;
                    var liId_index3 = nxtCount + 3;


                    if (nxtCount == 1) {
                        if (liId_index1 <= imgCounter) {
                            add_tag_li(liId_index1, nxtCount, "fb-nxt");
                        }
                        if (liId_index2 <= imgCounter) {
                            add_tag_li(liId_index2, liId_index1, "fb-nxt");
                        }
                    } else {
                        if (liId_index2 <= imgCounter) {
                            add_tag_li(liId_index2, liId_index1, "fb-nxt");
                        }
                        if (liId_index3 <= imgCounter) {
                            add_tag_li(liId_index3, liId_index2, "fb-nxt");
                        }
                    }
                    leftIsActive = false;
                    onCanvasClick();
                });

        //move to previous image on previous button click
        $('#fb-prev')
                .bind('click', function (event) {

                    zoomBtnClicked = false;
                    go_clicked_flag = false;
                    accordianClicked = false;

                    $("#canvasHolder1").css("cursor", "default");
                    stopAnimation();

                    var liId_index1, liId_index2;
                    liId_index1 = nxtCount - 2;
                    liId_index2 = nxtCount - 1;

                    if (nxtCount != 2 && $("#liId" + liId_index1).length == 0) {
                        add_tag_li(liId_index1, liId_index1 - 1, "fb-prev");
                    }
                    if ($("#liId" + liId_index2).length == 0) {
                        add_tag_li(liId_index2, liId_index2 - 1, "fb-prev");
                    }
                    leftIsActive = true;
                    onCanvasClick();
                });

        $('#fb-play-pause')
                .bind('click', function (event) {
                    zoomBtnClicked = false;
                    go_clicked_flag = false;
                    accordianClicked = false;

                    try {
                        /*   $(tempCanvas).unbind('mousemove');
                         $(tempCanvas).unbind('mouseout');
                         $(tempCanvas).unbind('mousedown');
                         $(tempCanvas).unbind('touchstart');*/

                        /*  $(document).unbind('mousemove');
                         $(document).unbind("touchmove");
                         $(document).unbind("mouseup");
                         $(document).unbind("touchend");*/
                    } catch (err) {
                        console.log("play pause unbind err", err);
                    }

                    $("#canvasHolder1").css("cursor", "default");
                    var someimage = document.getElementById('fb-play-pause');
                    var myimg = someimage.getElementsByTagName('img')[0];
                    var mysrc = myimg.src;
                    if (mysrc.indexOf("play-big.jpg") != -1) {
                        //myimg.src = "images/BookReader/pause-big.jpg";
                        myimg.src = imagePathJsonObject["pause-big"];
                        count = 0;
                        request = requestAnimationFrame(animationFun);
                        playing_flag = true;
                        play_pause_btn_clicked = true;
                    } else {
                        //myimg.src = "images/BookReader/play-big.jpg";
                        myimg.src = imagePathJsonObject["play-big"];
                        count = 0;
                        window.cancelAnimationFrame(request);
                        playing_flag = false;
                        play_pause_btn_clicked = false;
                    }
                });

        document.getElementById("fb-btn_div").addEventListener("mouseenter", function (event) {
            //console.log("mouse enter");
            try {
                /*  $(tempCanvas).unbind('mousemove', onCanvasHovering);
                 $(tempCanvas).unbind('mouseout', onCanvasHoverOut);
                 $(tempCanvas).unbind('mousedown', onCanvasActionStart);
                 $(tempCanvas).unbind('touchstart', onCanvasActionStart);*/

                /* $(document).unbind('mousemove', mousemove_handler);
                 $(document).unbind("touchmove", touchmove_handler);
                 $(document).unbind("mouseup", onCanvasActionStop);
                 $(document).unbind("touchend", onCanvasActionStop);*/
            } catch (err) {
                console.log("mouseenter err", err);
            }
        });
        document.getElementById("fb-btn_div").addEventListener("mouseleave", function (event) {
            /*console.log("mouse leave");
             console.log("zoomBtnClicked==>"+zoomBtnClicked);
             console.log("playing_flag==>"+playing_flag);*/
            try {

                if (zoomBtnClicked == false && playing_flag == false) {
                    /* $(tempCanvas).bind('mousemove', onCanvasHovering);
                     $(tempCanvas).bind('mouseout', onCanvasHoverOut);
                     $(tempCanvas).bind('mousedown', onCanvasActionStart);
                     $(tempCanvas).bind('touchstart', onCanvasActionStart);*/
                    /* $(document).bind('mousemove', mousemove_handler);
                     $(document).bind("touchmove", touchmove_handler);
                     $(document).bind("mouseup", onCanvasActionStop);
                     $(document).bind("touchend", onCanvasActionStop);*/
                }
            } catch (err) {
                console.log("mouseleave err", err);
            }
        });


        $('#canvasHolder').mouseenter(function (e) {
            load_next_two_prev_two(nxtCount - 1);
        });


        $("#fb-pg").bind("cut copy paste", function (e) {
            e.preventDefault();
        });


        $("#fb-pg").bind("change keyup", function () {
            var val = $(this).val();
            var split_val = val.split('-');
            nxtCount = parseInt(split_val[0]);

            if ($('#fb-pg').val().length == 0 || ($('#fb-pg').val() <= 0)) {

                document.getElementById("fb-nxt").style.opacity = 0.6;
                document.getElementById("fb-nxt").style.pointerEvents = "none";

                document.getElementById("fb-prev").style.opacity = 0.6;
                document.getElementById("fb-prev").style.pointerEvents = "none";

                document.getElementById("fb-zoom").style.opacity = 0.6;
                document.getElementById("fb-zoom").style.pointerEvents = "none";

                document.getElementById("fb-play-pause").style.opacity = 0.6;
                document.getElementById("fb-play-pause").style.pointerEvents = "none";

                $(":submit").attr("disabled", true);
                $(":submit").removeClass("btn_enabled");
                $(":submit").addClass("btn_disabled");
            }
        });

        $("#close_btn").bind("click", function (event) {
            //console.log("zoomBtnClicked"+zoomBtnClicked);
            //zoomBtnClicked=false;
            //2/12/2014 added to play the book if previously its being played before zoom
            //if (zoomBtnClicked) {
            if (play_pause_btn_clicked) {
                var someimage = document.getElementById('fb-play-pause');
                var myimg = someimage.getElementsByTagName('img')[0];
                var mysrc = myimg.src;
                //console.log("mySrc is" + mysrc);

                //myimg.src = "images/BookReader/pause-big.jpg";
                myimg.src = imagePathJsonObject["pause-big"];
                count = 0;
                request = requestAnimationFrame(animationFun);
                playing_flag = true;
            }
            close_popup();
        });

        /*next and previous zoom start*/
        $("#next_btn").bind('click', function (event) {
            //console.log("next_btn clicked");
            if (cur_img_index != imgCounter - 1) {
                cur_img_index = cur_img_index + 1;
            }
            var nextImgPath = imgPathArr[cur_img_index];
            //console.log("next img path", nextImgPath);
            $("#displayPages").find("img").attr('src', nextImgPath);

        });

        $("#prev_btn").bind('click', function (event) {
            //console.log("prev_btn clicked");
            if (cur_img_index != 0) {
                cur_img_index = cur_img_index - 1;
            }
            var prevImgPath = imgPathArr[cur_img_index];
            //console.log("prev img path", prevImgPath);
            $("#displayPages").find("img").attr('src', prevImgPath);
        });
        /*next and previous zoom start*/

        $('#fb-pg').on('keypress', function (e) {
            //if the letter is not digit then display error and don't type anything
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57) && (e.which != 13)) {

                //display error message
                $("#errmsg").html("Digits Only").show().fadeOut(1500);
                return false;
            }

            $('#fb-pg').on('keyup', function (e) {

                if ($('#fb-pg').val().length != 0 && ($('#fb-pg').val() != 0)) {
                    $(":submit").attr("disabled", false);
                    $(":submit").removeClass("btn_disabled");
                    $(":submit").addClass("btn_enabled");
                } else if ($('#fb-pg').val().length == 0 || ($('#fb-pg').val() == 0)) {
                    $(":submit").attr("disabled", true);
                    $(":submit").removeClass("btn_enabled");
                    $(":submit").addClass("btn_disabled");
                }
            });
        });


        $("#fb-pg").bind("click", function (event) {
            changeLanguage("english");
            var textbox_val = $(this).val();
            if (textbox_val.indexOf('-') != -1) {
                var split_textbox_val = textbox_val.split('-');
                $("#fb-pg").val(split_textbox_val[0]);
            }
        });


        $("#fb-zoom").bind("click", function (event) {
            //console.log("zoom clicked");
            zoomBtnClicked = true;
            accordianClicked = false;
            $("#canvasHolder1").css("cursor", "zoom-in");

            //2/12/2014 stop playing book if it is being played before zoom
            if (playing_flag) {
                var someimage = document.getElementById('fb-play-pause');
                var myimg = someimage.getElementsByTagName('img')[0];
                var mysrc = myimg.src;
                myimg.src = imagePathJsonObject["play-big"];
                count = 0;
                window.cancelAnimationFrame(request);
                playing_flag = false;
            }

        });

        $("#fb-go").bind("click", function (event /*,from*/) {
            /*var val=null;
             val=from;
             if(val==null){
             val = parseInt($('#fb-pg').val());
             }
             console.log("fb-go clicked"+from);*/
            go_clicked_flag = true;
            accordianClicked = false;
            var val = parseInt($('#fb-pg').val());
            //console.log("fb-go val=="+val);
            if (val > 1) {
                document.getElementById("fb-prev").style.opacity = 1.0;
                document.getElementById("fb-prev").style.pointerEvents = "auto";
            }

            if (val < imgCounter) {
                document.getElementById("fb-nxt").style.opacity = 1.0;
                document.getElementById("fb-nxt").style.pointerEvents = "auto";
            }

            document.getElementById("fb-zoom").style.opacity = 1.0;
            document.getElementById("fb-zoom").style.pointerEvents = "auto";

            document.getElementById("fb-play-pause").style.opacity = 1.0;
            document.getElementById("fb-play-pause").style.pointerEvents = "auto";

            if (val > imgCounter) {
                val = imgCounter;
            }

            var num1, num2;
            if ((val % 2) == 0) {
                num1 = val;
                num2 = val + 1;

                if (num2 < imgCounter) {
                    $('#fb-pg').val(num1 + "-" + num2);
                } else {
                    $('#fb-pg').val(num1);
                }
            } else {
                if (val != 1) {
                    num1 = val - 1;
                    num2 = val;
                    $('#fb-pg').val(num1 + "-" + num2);
                } else { //added 30-07-2015
                    $('#fb-pg').val(1);
                }
            }
            var selectBoxVal = $("#fb-pg").val();
            var split_selectBoxVal = selectBoxVal.split('-');
            cur_selectBoxVal = parseInt(split_selectBoxVal[0]);

            //console.log("fb-go prev_selectBoxVal==>"+prev_selectBoxVal);
            //console.log("fb-go cur_selectBoxVal==>"+cur_selectBoxVal);

            if (cur_selectBoxVal != prev_selectBoxVal) {
                document.getElementById("fb-prev").style.opacity = 0.6;
                document.getElementById("fb-prev").style.pointerEvents = "none";

                document.getElementById("fb-nxt").style.opacity = 0.6;
                document.getElementById("fb-nxt").style.pointerEvents = "none";


                if (prev_selectBoxVal != null) {
                    if (cur_selectBoxVal > prev_selectBoxVal) {
                        //console.log("inside cur_selectBoxVal > prev_selectBoxVal");
                        leftIsActive = false; //move to next
                        enable_disable_nxt_prev(num2);

                    } else {
                        //console.log("inside ELSE");
                        leftIsActive = true; //move to previous
                        if (val == 1) {
                            enable_disable_nxt_prev(1);
                        } else {
                            enable_disable_nxt_prev(num1);
                        }
                    }
                }
                //prev_selectBoxVal = cur_selectBoxVal; //commented on 30-07-2015

                nxtCount = cur_selectBoxVal;
                //console.log("fb-go nxtCount"+nxtCount);
                if (cur_selectBoxVal == 1) {
                    imageIndex = 2;
                }

                if (leftIsActive == false && cur_selectBoxVal != 1) {
                    imageIndex = cur_selectBoxVal - 2;
                }

                if (leftIsActive == true && cur_selectBoxVal != 1) {
                    imageIndex = cur_selectBoxVal + 2;
                }

                if ((nxtCount >= 1) && (nxtCount <= imgCounter)) {
                    add_tag_li(nxtCount, nxtCount - 1, "fb-go");
                    //console.log("fb-go nxtCount==" + nxtCount);
                    //console.log("fb-go  nxtCount - 1==" + nxtCount - 1);
                }

                var index_is = (nxtCount + 1);
                if ((index_is >= 1) && (index_is <= imgCounter)) {
                    add_tag_li(index_is, nxtCount, "fb-go");
                    //console.log("fb-go index_is==" + index_is);
                    //console.log("fb-go  nxtCount==" + nxtCount);
                }
                onCanvasClick();
                $(":submit").attr("disabled", true);
                $(":submit").removeClass("btn_enabled");
                $(":submit").addClass("btn_disabled");

                //accordianExpand();//commented 5-08-2015
            }
        });

        document.getElementById("fb-prev").style.opacity = "0.6";
        document.getElementById("fb-prev").style.pointerEvents = "none";

        //code added 25-04-2015 for button navigation
        document.onkeydown = function (e) {
            switch (e.keyCode) {
                case 37:
                    //alert('left');
                    $('#fb-prev').trigger('click');
                    break;
                    /*case 38:
                     alert('up');
                     break;*/
                case 39:
                    //alert('right');
                    $('#fb-nxt').trigger('click');
                    break;
                    /*case 40:
                     alert('down');
                     break;*/
            }
        };
    }


    var accordianClicked = false,
            accordianClicked_id = null;

    function findAllPageLi() {
        //console.log("findAllPageLi called");

        var my_id = null;
        var counter = 0;
        $("#recordTaggedPagesList").find('li').each(function () {
            //console.log("li is found");
            var current = $(this);
            //$('.active').removeClass('active');
            //$(this).addClass('active');

            current.find('a').each(function (index, element) {


                counter++;
                var b = $(this);
                //console.log("inside"+b.attr('id'));
                my_id = $(this).attr('id');
                //alert(my_id);

                try {
                    $("#" + my_id).bind('click', after_li_a_clicked);
                    //$("#"+my_id).live('click', after_li_a_clicked);
                    //console.log("my_id==" + my_id + "binded");
                } catch (err) {
                    console.log("EXE while binding" + err);
                }

            });
            //console.log("text=="+current.text()+"id=="+$(this).attr('id'));
        });

        //alert(phrases);
    }

    /*added on 11-08-2015 start*/
    var id_clicked = null;

    function after_li_a_clicked(event) {
        try {
            //console.log("after_li_a_clicked called");

            $('.active').removeClass('active'); //added on 5-08-2015
            $(this).addClass('active'); //added on 5-08-2015
            var id_is = $(this).attr('id');
            id_clicked = id_is;
            //console.log("id_clicked==" + id_clicked);
            unbind_events_exceptgivenid(id_is);
            var split_id_is = id_is.split('_');
            var val = parseInt(split_id_is[1]); //parseInt is very imp
            go_clicked_flag = true; //imp
            accordianClicked = true;
            accordianClicked_id = val;


            if (val > 1) {
                document.getElementById("fb-prev").style.opacity = 1.0;
                document.getElementById("fb-prev").style.pointerEvents = "auto";
            }

            if (val < imgCounter) {
                document.getElementById("fb-nxt").style.opacity = 1.0;
                document.getElementById("fb-nxt").style.pointerEvents = "auto";
            }

            document.getElementById("fb-zoom").style.opacity = 1.0;
            document.getElementById("fb-zoom").style.pointerEvents = "auto";

            document.getElementById("fb-play-pause").style.opacity = 1.0;
            document.getElementById("fb-play-pause").style.pointerEvents = "auto";

            if (val > imgCounter) {
                val = imgCounter;
            }

            var num1, num2;
            if ((val % 2) == 0) {
                num1 = val;
                num2 = val + 1;
                //console.log("num1==>"+num1+"num2==>"+num2);
                if (num2 < imgCounter) {
                    $('#fb-pg').val(num1 + "-" + num2);
                } else {
                    $('#fb-pg').val(num1);
                }
            } else {
                //console.log("inside else"+val);
                if (val != 1) {
                    num1 = val - 1;
                    num2 = val;
                    $('#fb-pg').val(num1 + "-" + num2);
                } else { //added 30-07-2015
                    $('#fb-pg').val(1);
                }
            }
            var selectBoxVal = $("#fb-pg").val();
            var split_selectBoxVal = selectBoxVal.split('-');
            cur_selectBoxVal = parseInt(split_selectBoxVal[0]);

            //console.log("cur_selectBoxVal==>"+cur_selectBoxVal+"prev_selectBoxVal==>"+prev_selectBoxVal);
            if (cur_selectBoxVal != prev_selectBoxVal) {
                document.getElementById("fb-prev").style.opacity = 0.6;
                document.getElementById("fb-prev").style.pointerEvents = "none";

                document.getElementById("fb-nxt").style.opacity = 0.6;
                document.getElementById("fb-nxt").style.pointerEvents = "none";


                if (prev_selectBoxVal != null) {
                    if (cur_selectBoxVal > prev_selectBoxVal) {
                        //console.log("findAllLi cur_selectBoxVal > prev_selectBoxVal");
                        leftIsActive = false; //move to next
                        enable_disable_nxt_prev(num2);

                    } else {
                        //console.log("findAllLi inside else");
                        leftIsActive = true; //move to previous
                        if (val == 1) {
                            enable_disable_nxt_prev(1);
                        } else {
                            enable_disable_nxt_prev(num1);
                        }
                    }
                }



                //prev_selectBoxVal = cur_selectBoxVal; //commented on 30-07-2015

                nxtCount = cur_selectBoxVal;
                if (cur_selectBoxVal == 1) {
                    imageIndex = 2;
                }

                if (leftIsActive == false && cur_selectBoxVal != 1) {
                    imageIndex = cur_selectBoxVal - 2;
                }

                if (leftIsActive == true && cur_selectBoxVal != 1) {
                    imageIndex = cur_selectBoxVal + 2;
                }


                if ((nxtCount >= 1) && (nxtCount <= imgCounter)) {
                    //console.log("findAllPageLi count1=="+nxtCount+"IMG=="+imgPathArr[nxtCount]);
                    var t = nxtCount - 1;
                    //console.log("findAllPageLi count2=="+t);
                    add_tag_li(nxtCount, nxtCount - 1, "findAllPageLi" + imgPathArr[t]);
                }

                var index_is = (nxtCount + 1);
                if ((index_is >= 1) && (index_is <= imgCounter)) {
                    //console.log("findAllPageLi count1=="+index_is+"IMG=="+imgPathArr[index_is]);
                    //console.log("findAllPageLi count2=="+nxtCount+"IMG=="+imgPathArr[nxtCount]);
                    add_tag_li(index_is, nxtCount, "findAllPageLi");
                }

                //add 2 previous images
                //			add_tag_li(nxtCount-2, nxtCount - 1,"findAllPageLi"+imgPathArr[t]);
                //
                //			//add 2 current images
                //			add_tag_li(nxtCount, nxtCount + 1,"findAllPageLi"+imgPathArr[t]);
                //
                //			//add 2 next images
                //			add_tag_li(nxtCount+2, nxtCount + 1,"findAllPageLi"+imgPathArr[t]);

                onCanvasClick();
                $(":submit").attr("disabled", true);
                $(":submit").removeClass("btn_enabled");
                $(":submit").addClass("btn_disabled");

            }
            setTimeout(afterTwoSeconds, 2000);
        } catch (err) {
            console.log("after_li_a_clicked EXE", err);
        }
    }
    /*added on 11-08-2015*/

    function afterTwoSeconds() {
        bind_events_exceptgivenid(id_clicked);
    }

    /*added on 11-08-2015 start*/
    function unbind_events_exceptgivenid(except_id) {
        try {
            //console.log("inside unbind");

            var my_id = null;
            var counter = 0;
            $('#recordTaggedPagesList').find('li').each(function () {
                var current = $(this);
                //$('.active').removeClass('active');
                //$(this).addClass('active');
                //console.log("inside each li");
                current.find('a').each(function (index, element) {


                    counter++;
                    var b = $(this);
                    //console.log("inside"+b.attr('id'));
                    my_id = $(this).attr('id');
                    if (my_id != except_id) {
                        $(this).addClass('inactive');
                        //$(this).addClass('disabled');
                        $("#" + my_id).unbind('click', after_li_a_clicked);

                    }
                });
            });
        } catch (err) {
            console.log("unbind_events_exceptgivenid EXE" + err);
        }

    }
    /*added on 11-08-2015 end*/

    /*added on 11-08-2015 start*/
    function bind_events_exceptgivenid(except_id) {
        //console.log("inside bind"+except_id);

        var my_id = null;
        var counter = 0;
        $('#recordTaggedPagesList').find('li').each(function () {
            var current = $(this);
            //$('.active').removeClass('active');
            //$(this).addClass('active');

            current.find('a').each(function (index, element) {


                counter++;
                var b = $(this);
                //console.log("inside"+b.attr('id'));
                my_id = $(this).attr('id');
                if (my_id != except_id) {
                    $(this).removeClass('inactive');
                    //$(this).removeClass('disabled');
                    $("#" + my_id).bind('click', after_li_a_clicked);
                    //console.log("after bind click");
                }
            });
        });

    }
    /*added on 11-08-2015 end*/

    //commenetd on 6-05-2015

    /*	function accordianExpand(autoExpand){//changed on 5-08-2015
     //console.log("accordianExpand called");
     var selectBoxVal = $("#fb-pg").val();
     var split_selectBoxVal = selectBoxVal.split('-');
     selectedVal = parseInt(split_selectBoxVal[0]);
     var id="page_"+selectedVal;
     $('#recordTaggedPagesList ul').each(function(){
     $(this).css("display","none");
     var ul_tobe_selected=$(this);

     $(this).find('li').each(function(){
     var current = $(this);
     current.find('a').each(function(index,element){
     if(id==$(this).attr('id')){
     //console.log("inside id EQUAL");
     ul_tobe_selected.css("display","block");
     }
     });
     });
     });

     //added on 5-08-2015
     if(autoExpand){
     //$('#recordTaggedPagesList ul:first').each(function(){
     //				var ul_tobe_selected_first=$(this);
     //				$(this).find('li:first').each(function(){
     //					var current_first = $(this);
     //					ul_tobe_selected_first.css("display","block");
     //					var fChild=$(this).children().first();
     //					fChild.addClass('active');
     //					console.log("fChild=="+fChild.attr('id'));
     //				});
     //			});

     $("#page_1").addClass('active');
     }

     }*/



    var onCornerMoveComplete;

    function clearTempCanvas() {
        tempContext.clearRect(0, 0, tempCanvas.width(), tempCanvas.height());
    }

    function onCanvasClick() {
        if (flipInProgress) {
            completeFlip();
            clearTempCanvas();

            invalidate();
        }
        var lastDirectionPage = leftIsActive ? imageIndex == 0 : imageIndex == sourceImagesLength - 2;
        if (lastDirectionPage) {
            return;
        }

        var pageIsNotLast = leftIsActive ? imageIndex > 0 : imageIndex < sourceImagesLength - 2;
        if (pageIsNotLast) {
            //console.log("leftIsActive="+leftIsActive);
            mousePosition = {
                top: options.pageHeight,
                left: leftIsActive ? options.pageWidth * 2 : 0
            };
            followerPosition = {
                left: leftIsActive ? 1 : options.pageWidth * 2 - 1,
                top: options.pageHeight - 1
            };

            /*console.log("mousePosition.top=="+mousePosition.top);
             console.log("mousePosition.left=="+mousePosition.left);
             console.log("followerPosition.top=="+followerPosition.top);
             console.log("followerPosition.left=="+followerPosition.left);*/
            flipInProgress = true;

            onCornerMoveComplete = getOnCornerMoveComplete(leftIsActive);

        }
        //console.log("flipInProgress"+flipInProgress);
    }
    var pageTurnedflag = false;

    function getOnCornerMoveComplete(leftPageIsActive) {
        //console.log("getOnCornerMoveComplete");
        var exceptid = null;
        pageTurnedflag = true;
        //console.log("getOnCornerMoveComplete leftPageIsActive",leftPageIsActive);
        if (playing_flag == false) {
            //console.log("nxtCount",nxtCount);
            //console.log("go_clicked_flag"+go_clicked_flag);
            if (!leftPageIsActive) {
                var num1;
                if (nxtCount == 1) {
                    num1 = nxtCount + 1;
                } else {
                    num1 = nxtCount + 2; //10
                }

                var num2 = num1 + 1; //11
                var fb_pg_val;
                if (num2 <= imgCounter) { //11<=10
                    fb_pg_val = num1 + "-" + num2;
                } else {
                    fb_pg_val = num1; //10
                }
                //console.log("fb_pg_val=="+fb_pg_val);
                //console.log("getOnCornerMoveComplete if num1==>"+num1);
                //console.log("getOnCornerMoveComplete if num2==>"+num2);
                //console.log("first enable_disable_nxt_prev NUM2=="+num2);

                if (go_clicked_flag == false) { //added 5-08-2015
                    enable_disable_nxt_prev(num1);

                }
                /*else{
                 unbind_events_exceptgivenid("page_"+num1);
                 exceptid=num1;
                 }*/
                /*if(accordianClicked){
                 tempContext.strokeStyle = '#0000CC';
                 tempContext.strokeRect(0, 0, options.pageWidth, options.pageHeight);
                 }*/
                // prev_selectBoxVal = num1; //commented 30-07-2015
            } else {

                var num1, fb_pg_val;
                if (nxtCount == 2) {
                    num1 = nxtCount - 1;
                    fb_pg_val = num1;
                } else {
                    num1 = nxtCount - 2;
                    var num2 = num1 + 1;
                    fb_pg_val = num1 + "-" + num2;
                }
                //console.log("second enable_disable_nxt_prev"+num1);
                //console.log("getOnCornerMoveComplete else==>"+num1);
                if (go_clicked_flag == false) { //added 5-08-2015
                    enable_disable_nxt_prev(num1);
                }
                /*else{
                 unbind_events_exceptgivenid("page_"+num1);
                 exceptid=num1;
                 }*/
                //prev_selectBoxVal = num1; //commented 30-07-2015
            }
            /*if(accordianClicked){
             tempContext.strokeStyle = '#0000CC';
             tempContext.strokeRect(options.pageWidth, 0, options.pageWidth, options.pageHeight);
             }*/
            prev_selectBoxVal = nxtCount;

            if (!go_clicked_flag) {
                $("#fb-pg").val(fb_pg_val).change();
            }

            //accordianExpand(false);//added 5-08-2015 //commenetd on 6-08-2015
        }

        return function () {
            imageIndex += leftPageIsActive ? -2 : 2;
            mousePosition = {
                left: leftPageIsActive ? 0 : options.pageWidth * 2,
                top: options.pageHeight
            };
            followerPosition = mousePosition;
            drawBackgroundPages(imageIndex);
            clearTempCanvas();
            dragging = false;
        };
    }

    //commented 21-08-2015
    /* function onCanvasActionStop(event) {
     try {
     var clickedElement = event.target;
     var clicked_tagName = clickedElement.tagName;
     //code to make flag true when mouse is clicked outside canvas area
     //also stop animation of playing pages

     //if we click on a tag or on unordered list
     if (clicked_tagName == "A" || clicked_tagName == "UL") {
     outerClick = true;
     stopAnimation();
     }

     //if clicked on tableContainer div
     if (clickedElement.id == "tableContainer") {
     outerClick = true;
     stopAnimation();
     }

     //if clicked on header or footer div
     if (clickedElement.className == "nav_bg" || clickedElement.className == "footer1") {
     outerClick = true;
     stopAnimation();
     }
     //console.log("onCanvasActionStop=="+outerClick)
     //when we are outside canvas area then dont flip the page
     //only return

     if (outerClick)
     return; // click occurred outside of visible page



     if (!flipInProgress) {
     dragging = false;

     if (leftIsActive ? imageIndex == 0 : imageIndex == sourceImagesLength - 2) {
     return;
     }

     var left;
     if (!browser_is_firefox) {
     //left = (event.pageX - $(event.target).offset().left) - $(holder).position().left;
     left = (event.pageX - $(event.target).parent().offset().left) - $(holder).position().left;
     } else {
     //left = event.pageX - $(holder).position().left;
     left = (event.pageX - $(event.target).parent().offset().left) - $(holder).position().left;
     }
     var actionDropArea = leftIsActive ? left > options.pageWidth : left < options.pageWidth;

     if (actionDropArea) {
     mousePosition = {
     left: leftIsActive ? options.spreadWidth : 0,
     top: options.pageHeight
     };
     flipInProgress = true;

     onCornerMoveComplete = getOnCornerMoveComplete(leftIsActive);
     } else {
     mousePosition = {
     left: leftIsActive ? options.cornerSide : options.spreadWidth - options.cornerSide,
     top: options.pageHeight - options.cornerSide
     };
     }
     }


     } catch (err) {
     console.log("onCanvasActionStop err", err);
     }
     }*/

    var nextMouseUpIsClick = false;

    //commented 21-08-2015
    /* function onCanvasActionStart(event) {

     //console.log("onCanvasActionStart leftIsActive="+leftIsActive);
     // mousedown occurred on canvas so now we are inside canvas
     //so make var outerClick false
     outerClick = false;
     //alert("onCanvasActionStart"+outerClick);
     load_next_two_prev_two(nxtCount - 1);
     go_clicked_flag = false;
     nextMouseUpIsClick = true;

     if (!flipInProgress) {
     var zeroPoint = $(holder).position();
     var relativePosition;

     if (!browser_is_firefox) {
     relativePosition = {
     top: (event.pageY - $(event.target).parent().offset().top) - zeroPoint.top - options.pageHeightOffset,
     left: (event.pageX - $(event.target).parent().offset().left) - zeroPoint.left
     };
     } else {
     relativePosition = {
     top: (event.pageY - $(event.target).parent().offset().top) - zeroPoint.top - options.pageHeightOffset,
     left: (event.pageX - $(event.target).parent().offset().left) - zeroPoint.left
     };
     }

     if (relativePosition.top >= 0 && relativePosition.top < options.pageHeight) {
     //console.log("1st if onCanvasActionStart");
     if (relativePosition.left >= 0 && relativePosition.left < options.pageWidth) {
     //console.log("1st if 1 onCanvasActionStart");
     mousePosition = {
     left: options.cornerSide,
     top: options.pageHeight - options.cornerSide
     };
     if (!leftIsActive) {
     leftIsActive = true;
     followerPosition = {
     left: 0,
     top: options.pageHeight
     };
     }
     } else if (relativePosition.left >= options.pageWidth && relativePosition.left < options.spreadWidth) {
     //console.log("2nd if 1 onCanvasActionStart");
     mousePosition = {
     left: options.spreadWidth - options.cornerSide,
     top: options.pageHeight - options.cornerSide
     };
     if (leftIsActive) {
     leftIsActive = false;
     followerPosition = {
     left: options.spreadWidth,
     top: options.pageHeight
     };
     }
     } else {
     //console.log("3rd if onCanvasActionStart");
     mousePosition = {
     left: leftIsActive ? 0 : options.spreadWidth,
     top: options.pageHeight
     };
     }

     event.preventDefault();
     dragging = true;
     }
     }

     var totIndex = imgCounter - 1;
     if (nxtCount < totIndex) {
     load_img_to_li(nxtCount, leftIsActive);
     }
     //console.log("onCanvasActionStart dragging",dragging);

     }*/

    /* function onCanvasHoverOut(event) {
     if (!dragging && !flipInProgress) {
     mousePosition = {
     left: leftIsActive ? 0 : options.spreadWidth,
     top: options.pageHeight
     };;
     }
     }*/

    //commented on 21-08-2015
    //var hovring_flag=false;
    /* function onCanvasHovering(event) {
     //console.log("onCanvasHovering called");
     try {

     go_clicked_flag = false;

     stopAnimation();
     nextMouseUpIsClick = false;

     if (!dragging && !flipInProgress) {
     //console.log("inside hovering true");
     //var zeroPoint = $(holder).position();
     //changed for problem which was coming for hovering after adding table
     var obj = $(holder);
     var childPos = obj.offset();
     var parentPos = obj.parent().offset();
     var zeroPoint = {
     top: childPos.top - parentPos.top,
     left: childPos.left - parentPos.left
     }
     var relativePosition;


     //working
     if (!browser_is_firefox) {
     relativePosition = {
     top: (((event.pageY - $(event.target).parent().offset().top)) - zeroPoint.top - options.pageHeightOffset),
     left: (((event.pageX - $(event.target).parent().offset().left)) - zeroPoint.left)
     };
     } else {
     relativePosition = {
     top: (event.pageY - $(event.target).parent().offset().top) - zeroPoint.top - options.pageHeightOffset,
     left: (event.pageX - $(event.target).parent().offset().left) - zeroPoint.left
     };
     }

     if (relativePosition.top >= 0 && relativePosition.top < options.pageHeight) {
     //console.log("1st if onCanvasHovering");
     if (relativePosition.left >= 0 && relativePosition.left < options.pageWidth) {
     //console.log("1st if 1 onCanvasHovering");
     mousePosition = {
     left: options.cornerSide,
     top: options.pageHeight - options.cornerSide
     };
     if (!leftIsActive) {
     leftIsActive = true;
     followerPosition = {
     left: 0,
     top: options.pageHeight
     };
     }
     } else if (relativePosition.left >= options.pageWidth && relativePosition.left < options.spreadWidth) {
     //console.log("1st if 2 onCanvasHovering");
     mousePosition = {
     left: options.spreadWidth - options.cornerSide,
     top: options.pageHeight - options.cornerSide
     };
     if (leftIsActive) {
     leftIsActive = false;
     followerPosition = {
     left: options.spreadWidth,
     top: options.pageHeight
     };
     }
     } else {
     //console.log("2nd if onCanvasHovering");
     mousePosition = {
     left: leftIsActive ? 0 : options.spreadWidth,
     top: options.pageHeight
     };
     }
     } else {
     //console.log("3rd if onCanvasHovering");
     mousePosition = {
     left: leftIsActive ? 0 : options.spreadWidth,
     top: options.pageHeight
     };
     }
     }
     } catch (err) {
     console.log("onCanvasHovering error", err);
     }

     //console.log("onCanvasHovering leftIsActive="+leftIsActive);
     }*/

    //commented 21-08-2015
    /* function mousemove_handler(event) {
     nextMouseUpIsClick = false;
     if (!flipInProgress) {
     if (dragging) {

     if (!browser_is_firefox) {
     mousePosition = {
     left: ((event.pageX - $(event.target).parent().offset().left) - $(holder).position().left),
     top: ((event.pageY - $(event.target).parent().offset().top) - options.pageHeightOffset + 70)
     };
     } else {
     mousePosition = {
     left: (event.pageX - $(event.target).parent().offset().left) - $(holder).position().left,
     top: (event.pageY - $(event.target).parent().offset().top) + 70
     };
     }
     }
     }
     }*/


    //commented 21-08-2015
    /* function touchmove_handler(event) {
     nextMouseUpIsClick = false;
     if (!flipInProgress) {
     if (dragging && event.originalEvent != null && event.originalEvent.touches.length == 1) {
     event.preventDefault();
     var touch = event.originalEvent.touches[0];
     mousePosition = {
     left: touch.screenX - $(holder).position().left,
     top: touch.screenY - options.pageHeightOffset + 70
     };
     }
     }
     }*/

    var selectBox_maintained_val;

    function afterCanvasClick(event) {
        //console.log("afterCanvasClick called");
        try {
            if (zoomBtnClicked) {
                var imgName_is = null;
                var selectBoxVal = $("#fb-pg").val();
                var split_selectBoxVal = selectBoxVal.split('-');
                var page_left = split_selectBoxVal[0];
                selectBox_maintained_val = page_left;
                var page_right = split_selectBoxVal[1];

                //console.log("page_left==" + page_left);
                //console.log("page_right==" + page_right);

                var x = (event.pageX - $(event.target).parent().offset().left);

                if (x < pageWidthMy) {
                    if (selectBoxVal != 1) {
                        imgName_is = imgPathArr[page_left - 1];
                        /*next and previous zoom start*/
                        cur_img_index = page_left - 1;
                        /*next and previous zoom end*/
                        display_popup(imgName_is);
                    }
                } else if (x > pageWidthMy) {
                    if (selectBoxVal != imgCounter) {
                        if (selectBoxVal == 1) {
                            imgName_is = imgPathArr[page_left - 1];
                            /*next and previous zoom start*/
                            cur_img_index = page_left - 1;
                            /*next and previous zoom end*/
                        } else {
                            imgName_is = imgPathArr[page_right - 1];
                            /*next and previous zoom start*/
                            cur_img_index = page_right - 1;
                            /*next and previous zoom end*/
                        }
                        display_popup(imgName_is);
                    }
                }
            }
        } catch (err) {
            console.log("afterCanvasClick", err);
        }
    }


    function display_popup(imgPath) {
        try {
            $("body").css("overflow-y", "hidden");
            $("div .audio_innerbox").hide();
            document.getElementById('fade').style.display = 'block';
            document.getElementById('light').style.display = 'block';
            document.getElementById('light').style.position = 'fixed';
            document.getElementById('fade').style.position = 'fixed';

            //console.log("W=="+getWindowWidth());
            //console.log("H=="+getWindowHeight());

            //console.log("content_wrapper W"+$("#content_wrapper").innerWidth());
            //console.log("content_wrapper H"+$("#content_wrapper").innerHeight());

            //changed for
            //added for scroll in popup start
//            var w = $("#content_wrapper").innerWidth() + "px";
//            var h = $("#content_wrapper").innerHeight() + "px";
//
//            document.getElementById('fade').style.width = w;
//            document.getElementById('fade').style.height = h;
//
//            document.getElementById('light').style.width = w;
//            document.getElementById('light').style.height = h;
            //added for scroll in popup end



            $('<div/>', {
                'class': 'viewerFlipbook',
                'id': 'displayPages'
            }).appendTo('#light');


            $('<div/>', {
                'id': 'containment-wrapper'
            }).appendTo('#displayPages');

            /*document.getElementById('containment-wrapper').style.width = getWindowWidth()+"px";
             document.getElementById('containment-wrapper').style.height = getWindowHeight()+"px";*/

            var imageViewerVar = $("#displayPages").iviewer({
                src: imgPath,
                update_on_resize: false,
                ui_disabled: true,
                zoom_animation: true,
                mousewheel: true,
                onDrag: function (ev, coords) {
                },
                onFinishLoad: function () {
                    imageViewerVar.iviewer('fit', 100);
                }
            });


            $('#displayPages').children("img").on('hover', function (e) {
//                $(this).css({
//                    'cursor': 'url(images/BookReader/openhand.cur), default'
//                });
                $(this).css({'cursor': 'url(' + imagePathJsonObject["openhand"] + '), default'});
            });

            $('#displayPages').children("img").on('mousedown', function (e) {
//                $(this).css({
//                    'cursor': 'url(images/BookReader/closedhand.cur), default'
//                });
                $(this).css({'cursor': 'url(' + imagePathJsonObject["closedhand"] + '), default'});
            });

            $('#displayPages').children("img").on('mouseup', function (e) {
//                $(this).css({
//                    'cursor': 'url(images/BookReader/openhand.cur), default'
//                });
                $(this).css({'cursor': 'url(' + imagePathJsonObject["openhand"] + '), default'});
            });

        } catch (err) {
            console.log("display_popup", err);
        }
    }

    var close_flag = false;

    function close_popup() {
        $("body").css("overflow-y", "auto");
        $("containment-wrapper").remove();
        $("#displayPages").remove();
        $("#displayPages > img").remove();
        $("#light").css("display", "none");
        $("#fade").css("display", "none");
        zoomBtnClicked = false;
        $("div .audio_innerbox").show();
        $("#canvasHolder1").css("cursor", "default");




        //console.log("close_btn cur_img_index==",cur_img_index);

        /*next and previous zoom start*/
        var actual_index = cur_img_index + 1;
        var page_val;
        if (actual_index == 1) {
            page_val = 1;
        } else if (actual_index == imgCounter) {
            page_val = imgCounter;
        } else {
            if ((actual_index % 2) == 0) {
                page_val_1 = actual_index;
                page_val_2 = actual_index + 1;

            } else {
                page_val_2 = actual_index;
                page_val_1 = actual_index - 1;
            }
            page_val = page_val_1 + "-" + page_val_2;
        }

        //console.log("page_val",page_val);
        $("#fb-pg").val(page_val).change();
        $("#fb-go").trigger("click");

        /*next and previous zoom end*/
        /*dragging=true;
         flipInProgress=true;
         console.log("dragging=="+dragging);
         console.log("flipInProgress=="+flipInProgress);*/

        //11-08-2015
        /*$(tempCanvas).bind('mousemove', onCanvasHovering);
         $(tempCanvas).bind('mouseout', onCanvasHoverOut);
         $(tempCanvas).bind('mousedown', onCanvasActionStart);
         $(tempCanvas).bind('touchstart', onCanvasActionStart);*/
        /*$(document).bind('mousemove', mousemove_handler);
         $(document).bind("touchmove", touchmove_handler);
         $(document).bind("mouseup", onCanvasActionStop);
         $(document).bind("touchend", onCanvasActionStop);*/

    }

    function completeFlip() {
        if (onCornerMoveComplete != null) {
            onCornerMoveComplete();
            onCornerMoveComplete = null;
        }
        flipInProgress = false;
    }

    function updateHandlePositions(imageIndex) {
        if (mousePosition == null) {
            return;
        }

        var followerDeltaTop = (mousePosition.top - followerPosition.top) * options.easing;
        var followerDeltaLeft = (mousePosition.left - followerPosition.left) * options.easing;
        //console.log("mousePosition.left=="+mousePosition.left);
        //console.log("followerPosition.left=="+followerPosition.left);
        //console.log("options.easing=="+options.easing);
        //console.log("followerDeltaTop0="+followerDeltaTop);
        //console.log("followerDeltaLeft0="+followerDeltaLeft);

        followerDeltaLeft = Math.abs(followerDeltaLeft) < 0.5 ? 0 : followerDeltaLeft;
        followerDeltaTop = Math.abs(followerDeltaTop) < 0.5 ? 0 : followerDeltaTop;

        //console.log("followerDeltaTop1="+followerDeltaTop);
        //console.log("followerDeltaLeft1="+followerDeltaLeft);

        if (followerDeltaLeft == 0 && followerDeltaTop == 0) {
            completeFlip();
            return;
        }

        followerPosition.top += followerDeltaTop;
        followerPosition.left += followerDeltaLeft;

        //console.log("followerDeltaTop2="+followerDeltaTop);
        //console.log("followerDeltaLeft2="+followerDeltaLeft);

        //			console.debug('mouse: x - ' + mousePosition.left + ', y - ' + mousePosition.top);
        //			console.debug('follower: x - ' + followerPosition.left + ', y - ' + followerPosition.top);

        var deltaX = followerPosition.left - options.pageWidth;
        var deltaY = spineBottomTop - followerPosition.top;
        //console.log("deltaX="+deltaX);
        //console.log("deltaY"+deltaY);
        var spineBottomToFollowerAngle = Math.atan2(deltaY, deltaX);
        //console.log("spineBottomToFollowerAngle="+spineBottomToFollowerAngle);
        var radiusLeft = Math.cos(spineBottomToFollowerAngle) * options.pageWidth + options.pageWidth;
        var radiusTop = spineBottomTop - Math.sin(spineBottomToFollowerAngle) * options.pageWidth;

        var distanceToFollower = Math.sqrt(
                (spineBottomTop - followerPosition.top) * (spineBottomTop - followerPosition.top) +
                (followerPosition.left - options.pageWidth) * (followerPosition.left - options.pageWidth)
                );
        var distanceToRadius = Math.sqrt(
                (spineBottomTop - radiusTop) * (spineBottomTop - radiusTop) +
                (radiusLeft - options.pageWidth) * (radiusLeft - options.pageWidth)
                );

        var cornerX;
        var cornerY;
        if (distanceToRadius < distanceToFollower) {
            cornerX = radiusLeft;
            cornerY = radiusTop;
        } else {
            cornerX = followerPosition.left;
            cornerY = followerPosition.top;
        }

        deltaX = spineTopX - cornerX;
        deltaY = cornerY;

        distanceToFollower = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
        var spineTopToFollowerAngle = Math.atan2(deltaY, deltaX);

        if (distanceToFollower > options.pageDiagonal) {
            var radius2HandleX = -Math.cos(spineTopToFollowerAngle) * options.pageDiagonal + options.pageWidth;
            var radius2HandleY = spineTopY + Math.sin(spineTopToFollowerAngle) * options.pageDiagonal;

            cornerX = radius2HandleX;
            cornerY = radius2HandleY;
        }

        var bisectorX = leftIsActive ? cornerX / 2 : ((options.pageWidth * 2 - cornerX) / 2 + cornerX);
        var bisectorY = (options.pageHeight - cornerY) / 2 + cornerY;

        var bisectorAngle = Math.atan2(options.pageHeight - bisectorY, leftIsActive ? bisectorX : options.pageWidth * 2 - bisectorX);
        var bisectorDeltaX = Math.tan(bisectorAngle) * (options.pageHeight - bisectorY);
        var bisectorTangetX = bisectorX + bisectorDeltaX * (leftIsActive ? 1 : -1);
        if (bisectorTangetX < 0) {
            bisectorTangetX = 0;
        }

        var pageAngleDeltaY = bisectorTangetY - cornerY;
        var pageAngleDeltaX = bisectorTangetX - cornerX;
        pageAngle = leftIsActive ? Math.atan2(-pageAngleDeltaY, -pageAngleDeltaX) : Math.atan2(pageAngleDeltaY, pageAngleDeltaX);

        var pageX = cornerX + options.pageWidth / 2 * Math.cos(pageAngle) * (leftIsActive ? -1 : 1) + options.pageHeight / 2 * Math.sin(pageAngle);
        var pageY = cornerY - options.pageHeight / 2 * Math.cos(pageAngle) + options.pageWidth / 2 * Math.sin(pageAngle) * (leftIsActive ? -1 : 1);

        var maskTanAngle = Math.atan2(options.pageHeight - bisectorY,
                bisectorX - bisectorTangetX);
        var maskAngle = 90 * (maskTanAngle / Math.abs(maskTanAngle)) - maskTanAngle * 180 / Math.PI;
        maskAngle = maskAngle / 180 * Math.PI;

        var xCoefficient = bisectorTangetY - bisectorY;
        var yCoefficient = bisectorX - bisectorTangetX;
        var freeCoefficient = bisectorTangetX * bisectorY - bisectorX * bisectorTangetY;

        var halfPageClipX = -(yCoefficient * options.pageHeight / 2 + freeCoefficient) / xCoefficient;
        var halfPageClipOffset = options.pageClipWidth / 2 / Math.cos(maskAngle);

        var maskLeft = halfPageClipX + halfPageClipOffset * (leftIsActive ? 1 : -1);
        var maskTop = options.pageHeight / 2;

        var anotherMaskLeft = maskLeft +
                (leftIsActive ?
                        -halfPageClipOffset * 3 :
                        halfPageClipOffset);

        //			anotherMaskLeft -= options.pageWidth * Math.cos(maskAngle);
        /*console.log("img index start=="+imageIndex);
         console.log("pageX=="+pageX);
         console.log("pageY=="+pageY);
         console.log("pageAngle=="+pageAngle);
         console.log("maskLeft=="+maskLeft);
         console.log("maskTop=="+maskTop);
         console.log("maskAngle=="+maskAngle);
         console.log("anotherMaskLeft=="+anotherMaskLeft);
         console.log("img index end="+imageIndex);*/

        drawPage(pageX, pageY, pageAngle, maskLeft, maskTop, maskAngle, anotherMaskLeft, imageIndex);
    }

    /*  function mirrorX(value) {
     return options.spreadWidth - value;
     }*/

    function drawPage(pageX, pageY, pageAngle, maskX, maskY, maskAngle, anotherMaskX, imgIndex) {
        tempContext.clearRect(0, 0, tempCanvas.width(), tempCanvas.height());
        //console.log("clear rect called");
        //console.log("pageX==",pageX);
        //console.log("pageY==",pageY);
        tempContext.save();

        tempContext.translate(maskX, maskY + 0);
        tempContext.rotate(maskAngle);

        tempContext.beginPath();
        tempContext.rect(-options.pageClipWidth / 2, -options.pageClipHeight / 2, options.pageClipWidth, options.pageClipHeight);
        tempContext.clip();

        tempContext.rotate(-maskAngle);
        tempContext.translate(-maskX, -maskY - 0);


        tempContext.translate(pageX, pageY + 0);

        tempContext.rotate(pageAngle);
        drawSource(tempContext, getFlipperImage(), -options.pageWidth / 2, -options.pageHeight / 2, imgIndex);

        tempContext.restore();
        tempContext.save();


        tempContext.translate(anotherMaskX, maskY + 0);
        tempContext.rotate(maskAngle);

        tempContext.beginPath();
        //tempContext.fillStyle = 'red';
        //tempContext.fillRect(0, -options.pageDiagonal, options.pageClipWidth, options.pageClipHeight);

        tempContext.rect(0, -options.pageDiagonal, options.pageClipWidth, options.pageClipHeight);

        //draw line for fold
        tempContext.clip();

        tempContext.rotate(-maskAngle);
        tempContext.translate(-anotherMaskX, -maskY - 0);

        tempContext.translate(0, 0);

        drawSource(tempContext, getAppearingImage(), leftIsActive ? 0 : options.pageWidth, 0, imgIndex);
        tempContext.restore();
    }

    //commented 21-08-2015
    /*function positionElement(element, x, y) {
     element = $(element);
     element.css({
     top: y - element.height() / 2 + options.pageHeightOffset,
     left: x - element.width() / 2
     });
     }*/

    function rotateElement(element, angle) {
        var angleInDegrees = angle / Math.PI * 180;
        $(element)
                .css({
                    '-moz-transform': 'rotate(' + angleInDegrees + 'deg)',
                    '-webkit-transform': 'rotate(' + angleInDegrees + 'deg)'
                });
    }

    function startInvalidationWorker() {
        var worker = function () {
            invalidate();
            setTimeout(worker, options.updateDelay);
        };

        worker();
    }

    function invalidate() {
        updateHandlePositions();
    }

    var imgCnt = 0;

    function initializeFlipper(element) {

        try {
            createCanvas(element);

            createHandles();

            activateMouseHandle();
            startInvalidationWorker();

            initializeDefaultImage();
            sourceImagesLength = imgCounter + 2;
            loadSingleImage(0);
            loadSingleImage(1);
            loadSingleImage(2);

        } catch (err) {
            console.log("initializeFlipper err", err);
        }

    }
    ;

    function drawBackgroundPages(imgIndex) {
        //console.log("drawBackgroundPages called");

        var leftImage = getLeftImage();
        var rightImage = getRightImage();
        //console.log("leftImage=="+leftImage);
        //console.log("rightImage=="+rightImage);
        if (leftImage != null) {
            //console.log("leftImage != null if");
            drawSource(context, leftImage, 0, 0, imgIndex);
        } else {
            //console.log("leftImage != null else");
            context.clearRect(0, 0, options.pageWidth, options.pageHeight);
        }
        if (rightImage != null) {
            //console.log("rightImage != null if");
            drawSource(context, rightImage, options.pageWidth, 0, imgIndex);
            //drawSource(context, rightImage, 0, 0, imgIndex);//CHANGED 9-03-2016
        } else {
            //console.log("rightImage != null else");
            context.clearRect(options.pageWidth, 0, options.pageWidth, options.pageHeight);
            //context.clearRect(options.pageWidth, 0, 0, options.pageHeight);//CHANGED 9-03-2016
        }
    }


    function drawSource(drawingContext, source, x, y, p) {

        //console.log("drawSource called",imageIndex);
        if (source != null && source.type != null && source.type.length > 0) {
            if ((source.type == 'image') && source.data != null) {
                if (source.isLoaded) {
                    //console.log("drawSource called",imageIndex);
                    //console.log("X",x,"Y",y,"leftIsActive"+leftIsActive);
                    //drawingContext.drawImage(source.data, x, y,source.data.width, source.data.height); //old
                    //console.log("accordianClicked=="+accordianClicked);
                    //console.log("accordianClicked_id=="+accordianClicked_id);
                    //console.log("leftIsActive=="+leftIsActive);
                    //console.log("source data"+source.data.src);
                    var wNew = document.getElementById("tempCanvas").width;
                    var hNew = document.getElementById("tempCanvas").height;
                    drawingContext.drawImage(source.data, x, y, options.pageWidth, options.pageHeight);
                    //console.log("x=="+x);
                    //console.log("y=="+y);

                    /*	if(accordianClicked){
                     drawingContext.strokeStyle = '#0000CC'; // some color/style
                     drawingContext.lineWidth = 4; // thickness
                     if(leftIsActive==false){
                     drawingContext.strokeRect(0, 0, options.pageWidth, options.pageHeight);
                     }else{
                     drawingContext.strokeRect(options.pageWidth, 0, options.pageWidth, options.pageHeight);
                     }
                     }else{*/
                    //Working
                    drawingContext.strokeStyle = '#000000'; // some color/style
                    drawingContext.lineWidth = 4; // thickness
                    drawingContext.strokeRect(x, y, options.pageWidth, options.pageHeight);
                    //}

                }
            } else if (source.type == 'background') {
                drawingContext.fillStyle = options.backgroundColor;
                drawingContext.fillRect(x, y, options.pageWidth, options.pageHeight);
            }
        }
    }

    var defaultImage = null;

    function initializeDefaultImage() {
        //console.log("initializeDefaultImage called");
        var defaultImageCanvas = $("<canvas></canvas>");
        defaultImageCanvas
                .attr('width', options.pageWidth)
                .attr('height', options.pageHeight);

        var context = defaultImageCanvas[0].getContext('2d');
        context.fillStyle = options.defaultPageColor;
        context.fillRect(0, 0, options.pageWidth, options.pageHeight);

        var imageData = new Image();
        imageData.onload = function () {
            defaultImage.isLoaded = true;
        };
        imageData.src = defaultImageCanvas[0].toDataURL();

        defaultImage = {
            type: 'image',
            data: imageData,
            isLoaded: false
        };
    }

    function getPageImage(index) {
        if (index == 0 || index == sourceImagesLength - 1) {
            return {
                type: 'background'
            };
        }

        return sourceImages[index - 1];
    }

    function loadSingleImage(index) {
        var source = imgPathArr[index];
        loadImage(source, function (loadedImage) {
            sourceImages[index] = {
                type: 'image',
                data: loadedImage,
                isLoaded: true
            };
            //commented 28/11/2014
            /*loadedImage.width=$(canvasHolder).width()/2;
             loadedImage.height=$(canvasHolder).height();*/

            if (index == imageIndex || index == imageIndex + 1) {
                drawBackgroundPages();
            }
        });
    }

    function add_tag_li(eleId, imgIndex, from) {
        //console.log("From=="+from+"imgIndex=="+imgIndex);
        if ($("#liId" + eleId).length == 0) {
            $('<li/>', {
                'id': 'liId' + (eleId)
            }).prepend("<img src=" + imgPathArr[imgIndex] + ">").appendTo('#lstImages');
            loadSingleImage(imgIndex);
        }
    }

    function load_img_to_li(nxtCout_val, leftIsActive_val) {
        var imgIndex = nxtCout_val;
        var totIndex = imgCounter - 1;
        var index = nxtCout_val - 1;

        if (!leftIsActive_val) {

            if (!leftIsActive) {
                var nextIndex1 = index + 1;
                var nextIndex2 = index + 2;
                if (nextIndex1 <= totIndex) {
                    add_tag_li(nextIndex1 + 1, nextIndex1, "load_img_to_li");
                }
                if (nextIndex2 <= totIndex) {
                    add_tag_li(nextIndex2 + 1, nextIndex2, "load_img_to_li");
                }
            }
        } else {
            var prevIndex1 = index - 1;
            var prevIndex2 = index - 2;
            if (prevIndex1 >= 0) {
                add_tag_li(prevIndex1 + 1, prevIndex1, "load_img_to_li");
            }

            if (prevIndex2 >= 0) {
                add_tag_li(prevIndex2 + 1, prevIndex2, "load_img_to_li");
            }
        }
    }

    function load_next_two_prev_two(curImgIndex) {
        var totIndex = imgCounter - 1;

        var nextIndex1 = curImgIndex + 1;
        var nextIndex2 = curImgIndex + 2;
        var nextIndex3 = curImgIndex + 3;

        if (nextIndex1 <= totIndex) {
            add_tag_li(nextIndex1 + 1, nextIndex1, "load_next_two_prev_two");

        }
        if (nextIndex2 <= totIndex) {
            add_tag_li(nextIndex2 + 1, nextIndex2, "load_next_two_prev_two");

        }
        if (nextIndex3 <= totIndex) {
            add_tag_li(nextIndex3 + 1, nextIndex3, "load_next_two_prev_two");

        }

        var prevIndex1 = curImgIndex - 1;
        var prevIndex2 = curImgIndex - 2;

        if (prevIndex1 >= 0) {
            add_tag_li(prevIndex1 + 1, prevIndex1, "load_next_two_prev_two");

        }
        if (prevIndex2 >= 0) {
            add_tag_li(prevIndex2 + 1, prevIndex2, "load_next_two_prev_two");

        }
    }

    function loadImage(url, onLoaded) {
        var image = new Image();
        image.onload = function () {
            onLoaded(image);
        };
        image.src = url;
    }

    function getLeftImage() {
        var leftI = getPageImage(imageIndex);
        //console.log("leftI=="+imgPathArr[imageIndex]);
        return getPageImage(imageIndex);
    }

    function getRightImage() {
        return getPageImage(imageIndex + 1);
        //console.log("RightI=="+imgPathArr[imageIndex+1]);
    }

    function getFlipperImage() {
        return getPageImage(leftIsActive ? imageIndex - 1 : imageIndex + 2);
    }

    function getAppearingImage() {
        return getPageImage(leftIsActive ? imageIndex - 2 : imageIndex + 3);
    }

    var sourceImagesLength = 0;
    var sourceImages = [];
    var imageIndex = 0;

    return this.each(function () {
        initializeFlipper(this);
    });
}; //bracket of pageFlipper



window.requestAnimationFrame = (function () {
    return window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
})();

var count, ctr = 0;

function animationFun() {
    if (count < 200) {
        count++;
    }

    if (count == 200) {
        prev_selectBoxVal = nxtCount;
        var num1, num2, fb_pg_val;

        if (((nxtCount + 2) > imgCounter) || ((nxtCount + 1) > imgCounter)) {
            num1 = 1;
            enable_disable_nxt_prev(1);
            fb_pg_val = 1;
        } else {
            if (nxtCount == 1) {
                num1 = nxtCount + 1;
            } else {
                num1 = nxtCount + 2;
            }
            var num2 = num1 + 1;
            enable_disable_nxt_prev(num2);
            fb_pg_val = num1 + "-" + num2;
        }
        nxtCount = num1;
        $("#fb-pg").val(fb_pg_val).change();
        $("#fb-go").trigger("click");
        count = 0;
    }
    request = requestAnimationFrame(animationFun);
}

function stopAnimation() {
    if (playing_flag == true) {
        count = 0;
        window.cancelAnimationFrame(request);
        playing_flag = false;
        var someimage = document.getElementById('fb-play-pause');
        var myimg = someimage.getElementsByTagName('img')[0];
        //myimg.src = "images/BookReader/play-big.jpg";
        myimg.src = imagePathJsonObject["play-big"];
    }
}

function enable_disable_nxt_prev(cnt_is) {
    var imgCounter1 = imgCounter + 1;
    //console.log("enable_disable_nxt_prev called cnt_is=="+cnt_is);
    //console.log("imgCounter==>"+imgCounter);
    //if ( cnt_is > imgCounter1) {
    //cnt_is=11
    //imgCounter=10
    if (cnt_is == imgCounter || cnt_is > imgCounter) {
        document.getElementById("fb-nxt").style.opacity = 0.6;
        document.getElementById("fb-nxt").style.pointerEvents = "none";
    } else {
        document.getElementById("fb-nxt").style.opacity = 1.0;
        document.getElementById("fb-nxt").style.pointerEvents = "auto";
    }


    if (cnt_is == 1) {
        document.getElementById("fb-prev").style.opacity = 0.6;
        document.getElementById("fb-prev").style.pointerEvents = "none";
    } else {
        document.getElementById("fb-prev").style.opacity = 1.0;
        document.getElementById("fb-prev").style.pointerEvents = "auto";
    }
}
