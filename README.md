# Instructions

## Fetch Source code
If you want to use `https`
```
$ git clone http://nvli-vm.pune.cdac.in:9999/nvli/nvli-themes.git
```
If you are using `ssh`

```
$ git clone git@nvli-vm.pune.cdac.in:nvli/nvli-themes.git
```
## Examples , Tutorials & Documentations
Some Useful Documentations and Guidelines for Using Git
  1.  Atlassian Git Tutorial ( **recommended**  )
    *   https://www.atlassian.com/git/

  2.  Git-SCM Official Git Documentation
    *   https://git-scm.com/doc

## Installation of Git CLI

If Git is not installed in you Machine then install it as follows
### Linux
#### RHEL Based Distributions can install with yum
```        
# yum install git
```
#### Debian Based Distros using apt-get
```
$ sudo apt-get install git
```
#### Windows User Download installer from [Here Official Website ](https://git-scm.com/download/win)

#### After Installation

  1. Please create ssh key .  Following link may help to create ssh key
    *  https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/

  2. Open Terminal (windos user open gitbash) for running some basic initial commands
     Setup your user name and email
     * https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup

## Setup One Time Configuration
Open `terminal` and hit following commands with your own user name and email. (Windows User, open `Git Bash` program)
```
$ git config --global user.name "Your Name"
$ git config --global user.email  "your.email@example.com"
```

After installations of `git` and setting up `user.name` and `user.email`, you are now ready to fetch the repository and start contributing.
